source 'https://rubygems.org'

ruby '2.3.0'
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.0', '>= 5.0.0.1'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.18'
# Use Puma as the app server
gem 'puma', '~> 3.0'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# See https://github.com/rails/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks'
gem 'jquery-turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
gem 'capistrano-rails', group: :development

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console'
  gem 'listen', '~> 3.0.5'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'rack-mini-profiler'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

## Font Awesome
gem 'font-awesome-rails'

## Devise is a flexible authentication solution for Rails based on warden it.
gem 'devise'

## This is a Rails gem for conveniently manage multiple nested models in a single form. 
gem "nested_form"

## jQuery Validation
gem 'jquery-validation-rails'

## BreadcrumbsOnRails is a simple Ruby on Rails plugin for creating and managing a breadcrumb navigation for a Rails project. 
gem "breadcrumbs_on_rails"

## Paperclip is intended as an easy file attachment library for ActiveRecord.
gem "paperclip"
## AWS S3
gem 'aws-sdk', '< 2.0'

## FriendlyId is the "Swiss Army bulldozer" of slugging and permalink plugins for Active Record.
gem 'friendly_id', '~> 5.1.0'

## Paranoia is a re-implementation of acts_as_paranoid for Rails
gem "paranoia", "~> 2.2"

## Decent Exposure
gem 'decent_exposure', '3.0.0'


## Localize server times in your view.
gem 'local_time'

## For Payment
gem 'authorize-net'
gem 'activemerchant'
gem 'authorizenet'

## If you need to send some data to your js files and you don't want to do this with long way through views and parsing
gem 'gon'

## Choosen JS - Chosen is a library for making long, unwieldy select boxes more user friendly.
gem 'chosen-rails'

## Multiselect dropdown
gem 'bootstrap-multiselect-rails'

## Inline Edit
gem 'bootstrap-editable-rails'

## Whenever
gem 'whenever', require: false

## Add a comment summarizing the current schema to the top or bottom of each of your models
gem 'annotate'

## For Toastr
gem 'toastr-rails'

## ClockPicker
gem 'clockpicker-rails'

## Ancestry is a gem/plugin that allows the records of a Ruby on Rails ActiveRecord model to be organised as a tree structure.
gem 'ancestry'

## For notifier for sending notification when errors occurs.
gem 'exception_notification'

## Remotipart is a Ruby on Rails gem enabling AJAX file uploads with jQuery
gem 'remotipart', '~> 1.2'

## Fullcalendar View
gem 'fullcalendar-rails'
gem 'momentjs-rails'

## Generate PDF with Prawn
gem 'prawn-rails'
gem 'wicked_pdf'
gem 'wkhtmltopdf-binary'