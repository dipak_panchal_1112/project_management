Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  ## Root Path
  root to: 'welcome#index'

  ## Devise Authentication 
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations',
    confirmations: 'users/confirmations',
    passwords: 'users/passwords',
    unlocks: 'users/unlocks'
  }

  ## ------------------------------------------------------------------------------
  ## Organizations
  ## ------------------------------------------------------------------------------

  resources :organizations do
  	collection do
  		get :getOrganization
  	end
  end

  ## ------------------------------------------------------------------------------
  ## User Routes
  ## ------------------------------------------------------------------------------

  resources :users do
    collection do
      ## Non Authentication Routes
      get :getUser
      get :existUser
      get :lockUser
      get :profile
      get :getPassword
      get :creditCardValidation
      get :lock_screen
    end
  end

  resources :invitations do
    collection do
      get :existMember
      get :accept
      get :thank_you
      post :accept_invitation
    end
  end

  resources :projects do
    collection do
      get :getProNameUniq ## Get Uniq Project Name
    end
    member do
      post :invitation
    end
    resources :clients, except: [:index]
  end

  ## Dashboard Routes
  get '/dashboard' => 'users#dashboard', as: :user_dashboard
  get '/admin/dashboard' => 'admin#dashboard', as: :admin_dashboard

  ## ------------------------------------------------------------------------------
  ## Secure Routes
  ## ------------------------------------------------------------------------------
  namespace :secure do
    resources :projects do
      resources :tasks do
        collection do 
          post :store_attachments
        end
        member do
          get :subtask
        end
        resources :task_attachments, only: [:destroy] do
          get :download
        end

        resources :assigns, only: [:new, :create]
        resources :watchers, only: [:create, :new]
      end
      member do
        get :reports
        resources :agile_boards, only: [:index] do
          collection do
            post :add_task
          end
        end
      end
      resources :comments, except: [:index, :show]
    end    
  end

  ## ------------------------------------------------------------------------------
  ## HR department
  ## ------------------------------------------------------------------------------
  
  resources :holidays do
    collection do
      get :getExistHoliday
      get :import
      get :calendar
      post :import_holiday
    end
  end

  resources :employees do
    collection do
      put :employee_document_update
    end
    resources :employee_documents, only: [:destroy] do
      get :download
    end
  end

  resources :letters do
    member do
      get :generate_pdf
      patch :download_pdf
      get :pdf_viewer      
    end
    collection do
      post :user_info
    end
  end

  ## ------------------------------------------------------------------------------
  ## Admin Routes
  ## ------------------------------------------------------------------------------

  namespace :admin do
    resources :organizations
    
    resources :plans do
      collection do
        get 'getPlan'
      end
    end

    resources :transactions, only: [:index, :show]
  end

  ## Non Authenticate Routes for authorize silent post
  post '/transactions/authorize_silent_post' => 'transactions#authorize_silent_post'
  get '/thank-you' => 'welcome#thank_you', as: :thank_you
end
