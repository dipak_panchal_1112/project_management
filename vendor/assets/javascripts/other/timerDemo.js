var Example1 = new (function() {
  var $stopwatch, // Stopwatch element on the page
      incrementTime = 1000, // Timer speed in milliseconds
      currentTime = 1, // Current time in hundredths of a second
  updateTimer = function() {
    $stopwatch.html(formatTime(currentTime));
    currentTime += incrementTime ;
  },
  init = function() {
    $stopwatch = $('.watch-'+ gon.task_slug);
    Example1.Timer = $.timer(updateTimer, incrementTime, false);
  };
  this.resetStopwatch = function() {
    currentTime = 0;
    this.Timer.stop().once();
  };
  $(init);
});

function formatTime(duration) {
  var seconds = parseInt((duration/1000)%60),
    minutes = parseInt((duration/(1000*60))%60),
    hours = parseInt((duration/(1000*60*60))%24);

  hours = (hours < 10) ? "0" + hours : hours;
  minutes = (minutes < 10) ? "0" + minutes : minutes;
  seconds = (seconds < 10) ? "0" + seconds : seconds;
  
  return hours + ":" + minutes + ":" + seconds;
}
