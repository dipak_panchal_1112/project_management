# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161221182000) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "certificates", force: :cascade do |t|
    t.integer  "profile_id"
    t.string   "name"
    t.integer  "year",       default: 0
    t.integer  "month",      default: 0
    t.string   "klass"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["profile_id"], name: "index_certificates_on_profile_id", using: :btree
  end

  create_table "clients", force: :cascade do |t|
    t.integer  "project_id"
    t.string   "fullname"
    t.string   "address"
    t.string   "email"
    t.string   "phone_number"
    t.string   "mobile_number"
    t.string   "email2"
    t.string   "skype"
    t.integer  "user_id"
    t.integer  "organization_id"
    t.string   "company_name"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["organization_id"], name: "index_clients_on_organization_id", using: :btree
    t.index ["project_id"], name: "index_clients_on_project_id", using: :btree
    t.index ["user_id"], name: "index_clients_on_user_id", using: :btree
  end

  create_table "comments", force: :cascade do |t|
    t.integer  "task_id"
    t.integer  "user_id"
    t.string   "attachment_ids"
    t.text     "content"
    t.integer  "project_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["project_id"], name: "index_comments_on_project_id", using: :btree
    t.index ["task_id"], name: "index_comments_on_task_id", using: :btree
    t.index ["user_id"], name: "index_comments_on_user_id", using: :btree
  end

  create_table "employee_documents", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "document_file_name"
    t.string   "document_content_type"
    t.integer  "document_file_size"
    t.datetime "document_updated_at"
    t.string   "document_name"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "organizations_user_id"
    t.index ["organizations_user_id"], name: "index_employee_documents_on_organizations_user_id", using: :btree
    t.index ["user_id"], name: "index_employee_documents_on_user_id", using: :btree
  end

  create_table "histories", force: :cascade do |t|
    t.integer  "task_id"
    t.integer  "user_id"
    t.text     "message"
    t.string   "change_type"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["task_id"], name: "index_histories_on_task_id", using: :btree
    t.index ["user_id"], name: "index_histories_on_user_id", using: :btree
  end

  create_table "holidays", force: :cascade do |t|
    t.string   "name"
    t.date     "hdate"
    t.integer  "htype"
    t.integer  "organization_id"
    t.boolean  "archive"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["organization_id"], name: "index_holidays_on_organization_id", using: :btree
  end

  create_table "invitations", force: :cascade do |t|
    t.string   "token"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invited_by_user_id"
    t.integer  "invited_by_organization_id"
    t.string   "invite_email"
    t.integer  "role_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["invited_by_organization_id"], name: "index_invitations_on_invited_by_organization_id", using: :btree
    t.index ["invited_by_user_id"], name: "index_invitations_on_invited_by_user_id", using: :btree
    t.index ["token"], name: "index_invitations_on_token", unique: true, using: :btree
  end

  create_table "letters", force: :cascade do |t|
    t.integer  "organization_id"
    t.text     "description"
    t.string   "title"
    t.boolean  "is_default",      default: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "slug"
    t.index ["organization_id"], name: "index_letters_on_organization_id", using: :btree
    t.index ["slug"], name: "index_letters_on_slug", unique: true, using: :btree
  end

  create_table "organizations", force: :cascade do |t|
    t.string   "name",                  default: "",    null: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.datetime "deleted_at"
    t.string   "title"
    t.string   "address"
    t.string   "contact_number"
    t.string   "email"
    t.string   "website"
    t.integer  "status_id",             default: 1
    t.string   "slug"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.text     "about_us"
    t.integer  "plan_id"
    t.string   "payment_status"
    t.integer  "subscription_id"
    t.boolean  "track_timing_for_task", default: false
    t.index ["deleted_at"], name: "index_organizations_on_deleted_at", using: :btree
    t.index ["name"], name: "index_organizations_on_name", unique: true, using: :btree
    t.index ["payment_status"], name: "index_organizations_on_payment_status", using: :btree
    t.index ["plan_id"], name: "index_organizations_on_plan_id", using: :btree
    t.index ["slug"], name: "index_organizations_on_slug", using: :btree
  end

  create_table "organizations_users", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "organization_id"
    t.integer  "role_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "status_id",       default: 1
    t.index ["organization_id"], name: "index_organizations_users_on_organization_id", using: :btree
    t.index ["status_id"], name: "index_organizations_users_on_status_id", using: :btree
    t.index ["user_id"], name: "index_organizations_users_on_user_id", using: :btree
  end

  create_table "plans", force: :cascade do |t|
    t.string   "name"
    t.float    "price",      default: 0.0
    t.string   "title"
    t.integer  "users",      default: 0
    t.integer  "storage",    default: 0
    t.boolean  "primary",    default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_plans_on_deleted_at", using: :btree
  end

  create_table "profile2s", force: :cascade do |t|
    t.integer  "profile_id"
    t.string   "company_name"
    t.text     "reason_for_resignation"
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["profile_id"], name: "index_profile2s_on_profile_id", using: :btree
  end

  create_table "profiles", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "phone_number"
    t.string   "mobile_number"
    t.string   "home_number"
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.string   "zipcode"
    t.string   "country"
    t.text     "about_me"
    t.date     "joining_date"
    t.date     "birth_date"
    t.date     "marriage_date"
    t.date     "resign_date"
    t.string   "website"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "designation"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.integer  "organization_id"
    t.integer  "organizations_user_id"
    t.string   "firstname"
    t.string   "lastname"
    t.string   "employee_code"
    t.integer  "gender"
    t.string   "education_status"
    t.string   "blood_group"
    t.string   "personal_email"
    t.string   "personal_skype"
    t.string   "skype"
    t.integer  "appointment_type",         default: 1
    t.string   "bank_name"
    t.string   "account_number"
    t.boolean  "same_as_current_address",  default: false
    t.string   "permanent_address"
    t.string   "permanent_city"
    t.string   "permanent_state"
    t.string   "permanent_country"
    t.string   "permanent_zipcode"
    t.string   "parent_name"
    t.string   "relationship_with_parent"
    t.string   "parent_phone_number"
    t.string   "parent_address"
    t.string   "parent_city"
    t.string   "parent_state"
    t.string   "parent_country"
    t.string   "parent_zipcode"
    t.string   "ifsc_code"
    t.index ["organization_id"], name: "index_profiles_on_organization_id", using: :btree
    t.index ["organizations_user_id"], name: "index_profiles_on_organizations_user_id", using: :btree
    t.index ["user_id"], name: "index_profiles_on_user_id", using: :btree
  end

  create_table "projects", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.date     "start_date"
    t.date     "end_date"
    t.float    "total_hour",      default: 0.0
    t.text     "url"
    t.integer  "status_id",       default: 1
    t.integer  "organization_id"
    t.string   "slug"
    t.string   "key"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.text     "keywords"
    t.index ["key"], name: "index_projects_on_key", using: :btree
    t.index ["organization_id"], name: "index_projects_on_organization_id", using: :btree
    t.index ["slug"], name: "index_projects_on_slug", using: :btree
    t.index ["status_id"], name: "index_projects_on_status_id", using: :btree
  end

  create_table "projects_users", force: :cascade do |t|
    t.integer  "project_id"
    t.integer  "user_id"
    t.integer  "organization_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["organization_id"], name: "index_projects_users_on_organization_id", using: :btree
    t.index ["project_id"], name: "index_projects_users_on_project_id", using: :btree
    t.index ["user_id"], name: "index_projects_users_on_user_id", using: :btree
  end

  create_table "subscriptions", force: :cascade do |t|
    t.integer  "plan_id"
    t.date     "next_renewal_date"
    t.string   "credit_card_encrypted"
    t.string   "card_expire"
    t.string   "subscription_id"
    t.date     "start_date"
    t.date     "end_date"
    t.string   "status"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "task_attachments", force: :cascade do |t|
    t.integer  "task_id"
    t.integer  "user_id"
    t.integer  "project_id"
    t.string   "document_file_name"
    t.string   "document_content_type"
    t.integer  "document_file_size"
    t.datetime "document_updated_at"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "token"
    t.index ["project_id"], name: "index_task_attachments_on_project_id", using: :btree
    t.index ["task_id"], name: "index_task_attachments_on_task_id", using: :btree
    t.index ["user_id"], name: "index_task_attachments_on_user_id", using: :btree
  end

  create_table "tasks", force: :cascade do |t|
    t.string   "slug"
    t.integer  "project_id"
    t.integer  "issue_type_id", default: 3
    t.text     "summary"
    t.text     "description"
    t.date     "due_date"
    t.integer  "status_id",     default: 1
    t.integer  "priority",      default: 1
    t.integer  "resolution"
    t.integer  "reporter_id"
    t.integer  "creator_id"
    t.integer  "total_min",     default: 0
    t.integer  "task_number",   default: 0
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "estimate_time"
    t.string   "ancestry"
    t.index ["ancestry"], name: "index_tasks_on_ancestry", using: :btree
    t.index ["creator_id"], name: "index_tasks_on_creator_id", using: :btree
    t.index ["issue_type_id"], name: "index_tasks_on_issue_type_id", using: :btree
    t.index ["priority"], name: "index_tasks_on_priority", using: :btree
    t.index ["project_id"], name: "index_tasks_on_project_id", using: :btree
    t.index ["reporter_id"], name: "index_tasks_on_reporter_id", using: :btree
    t.index ["slug"], name: "index_tasks_on_slug", using: :btree
    t.index ["status_id"], name: "index_tasks_on_status_id", using: :btree
  end

  create_table "tasks_users", force: :cascade do |t|
    t.integer  "task_id"
    t.integer  "user_id"
    t.integer  "total_min",  default: 0
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["task_id"], name: "index_tasks_users_on_task_id", using: :btree
    t.index ["user_id"], name: "index_tasks_users_on_user_id", using: :btree
  end

  create_table "transactions", force: :cascade do |t|
    t.integer  "organization_id"
    t.integer  "subscription_id"
    t.string   "subscription_number"
    t.float    "amount",               default: 0.0
    t.string   "status"
    t.string   "response_code"
    t.string   "response_reason_text"
    t.string   "transaction_id"
    t.text     "response"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.index ["organization_id"], name: "index_transactions_on_organization_id", using: :btree
    t.index ["status"], name: "index_transactions_on_status", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "firstname"
    t.string   "lastname"
    t.string   "username",               default: "", null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "slug"
    t.datetime "deleted_at"
    t.integer  "organization_id"
    t.integer  "status_id",              default: 1
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
    t.index ["deleted_at"], name: "index_users_on_deleted_at", using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["slug"], name: "index_users_on_slug", using: :btree
    t.index ["status_id"], name: "index_users_on_status_id", using: :btree
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree
  end

  create_table "watchers", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "task_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["task_id"], name: "index_watchers_on_task_id", using: :btree
    t.index ["user_id"], name: "index_watchers_on_user_id", using: :btree
  end

  create_table "work_logs", force: :cascade do |t|
    t.integer  "task_id"
    t.integer  "user_id"
    t.integer  "organization_id"
    t.integer  "log_time",        default: 0
    t.integer  "status_id"
    t.text     "message"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["organization_id"], name: "index_work_logs_on_organization_id", using: :btree
    t.index ["task_id"], name: "index_work_logs_on_task_id", using: :btree
    t.index ["user_id"], name: "index_work_logs_on_user_id", using: :btree
  end

end
