class CreateComments < ActiveRecord::Migration[5.0]
  def change
    create_table :comments do |t|
      t.integer :task_id
      t.integer :user_id
      t.string :attachment_ids
      t.text :content
      t.integer :project_id
      t.timestamps
    end
    add_index :comments, :task_id
    add_index :comments, :user_id
    add_index :comments, :project_id
  end
end
