class CreateTasks < ActiveRecord::Migration[5.0]
  def change
    create_table :tasks do |t|
    	t.string :slug
      t.integer :project_id
      t.integer :issue_type_id, default: 3
      t.text :summary
      t.text :description
      t.date :due_date
      t.integer :status_id, default: 1
      t.integer :priority, default: 1
      t.integer :resolution
      t.integer :reporter_id
      t.integer :creator_id
      t.integer :total_min, default: 0
      t.integer :task_number, default: 0
      t.timestamps
    end
    add_index :tasks, :project_id
    add_index :tasks, :issue_type_id
    add_index :tasks, :status_id
    add_index :tasks, :priority
    add_index :tasks, :reporter_id
    add_index :tasks, :creator_id
    add_index :tasks, :slug
  end
end
