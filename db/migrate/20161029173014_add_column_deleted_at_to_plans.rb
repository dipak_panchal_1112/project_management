class AddColumnDeletedAtToPlans < ActiveRecord::Migration[5.0]
  def change
    add_column :plans, :deleted_at, :datetime
    add_index :plans, :deleted_at
  end
end
