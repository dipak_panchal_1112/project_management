class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.string :name
      t.text :description
      t.date :start_date
      t.date :end_date
      t.float :total_hour, default: 0.0
      t.text :url
      t.integer :status_id, default: 1
      t.integer :organization_id
      t.string :slug
      t.string :key      
      t.timestamps
    end
    add_index :projects, :organization_id
    add_index :projects, :status_id
    add_index :projects, :slug
    add_index :projects, :key
  end
end
