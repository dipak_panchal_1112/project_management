class CreateWorkLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :work_logs do |t|
      t.integer :task_id
      t.integer :user_id
      t.integer :organization_id
      t.integer :log_time, default: 0
      t.integer :status_id
      t.text :message
      t.timestamps
    end
    add_index :work_logs, :task_id
    add_index :work_logs, :user_id
    add_index :work_logs, :organization_id
  end
end
