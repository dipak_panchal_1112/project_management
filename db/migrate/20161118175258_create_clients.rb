class CreateClients < ActiveRecord::Migration[5.0]
  def change
    create_table :clients do |t|
      t.integer :project_id
      t.string :fullname
      t.string :address
      t.string :email
      t.string :phone_number
      t.string :mobile_number
      t.string :email2
      t.string :skype
      t.integer :user_id
      t.integer :organization_id
      t.string :company_name
      t.timestamps
    end
    add_index :clients, :project_id
    add_index :clients, :user_id
    add_index :clients, :organization_id
  end
end
