class CreateCertificates < ActiveRecord::Migration[5.0]
  def change
    create_table :certificates do |t|
      t.integer :profile_id
      t.string :name
      t.integer :year, default: 0
      t.integer :month, default: 0
      t.string :klass
      t.timestamps
    end
    add_index :certificates, :profile_id
  end
end
