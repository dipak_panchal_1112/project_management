class RemoveColumnPrevCompanyNameToProfiles < ActiveRecord::Migration[5.0]
  def change
  	remove_column :profiles, :prev_company_name
  	remove_column :profiles, :prev_company_exp
  end
end
