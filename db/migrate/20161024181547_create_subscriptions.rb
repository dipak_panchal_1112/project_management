class CreateSubscriptions < ActiveRecord::Migration[5.0]
  def change
    create_table :subscriptions do |t|
      t.integer :plan_id
      t.date :next_renewal_date
      t.string :credit_card_encrypted
      t.string :card_expire
      t.string :subscription_id
      t.date :start_date
      t.date :end_date
      t.string :status      
      t.timestamps
    end
  end
end
