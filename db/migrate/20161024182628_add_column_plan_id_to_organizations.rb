class AddColumnPlanIdToOrganizations < ActiveRecord::Migration[5.0]
  def change
    add_column :organizations, :plan_id, :integer
    add_column :organizations, :payment_status, :string
    add_column :organizations, :subscription_id, :integer
    add_index :organizations, :plan_id
    add_index :organizations, :payment_status    
  end
end
