class CreateEmployeeDocuments < ActiveRecord::Migration[5.0]
  def change
    create_table :employee_documents do |t|
      t.integer :user_id
      t.attachment :document
      t.string :document_name
      t.integer :organization_id
      t.timestamps
    end
    add_index :employee_documents, :user_id
    add_index :employee_documents, :organization_id
  end
end
