class AddColumnStatusIdToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :status_id, :integer, default: 1
    add_index :users, :status_id
  end
end
