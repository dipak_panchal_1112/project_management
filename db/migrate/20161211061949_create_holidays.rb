class CreateHolidays < ActiveRecord::Migration[5.0]
  def change
    create_table :holidays do |t|
      t.string :name
      t.date :hdate
      t.integer :htype
      t.integer :organization_id
      t.boolean :archive
      t.timestamps
    end
    add_index :holidays, :organization_id
  end
end
