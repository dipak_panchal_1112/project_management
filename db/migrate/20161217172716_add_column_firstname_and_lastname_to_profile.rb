class AddColumnFirstnameAndLastnameToProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :profiles, :firstname, :string
    add_column :profiles, :lastname, :string
  end
end
