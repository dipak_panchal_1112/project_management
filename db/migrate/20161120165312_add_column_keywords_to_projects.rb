class AddColumnKeywordsToProjects < ActiveRecord::Migration[5.0]
  def change
    add_column :projects, :keywords, :text
  end
end
