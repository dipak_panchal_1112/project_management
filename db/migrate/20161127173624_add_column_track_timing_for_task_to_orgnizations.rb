class AddColumnTrackTimingForTaskToOrgnizations < ActiveRecord::Migration[5.0]
  def change
  	add_column :organizations, :track_timing_for_task, :boolean, default: false
  end
end
