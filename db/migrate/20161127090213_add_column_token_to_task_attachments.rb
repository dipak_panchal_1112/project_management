class AddColumnTokenToTaskAttachments < ActiveRecord::Migration[5.0]
  def change
    add_column :task_attachments, :token, :string
  end
end
