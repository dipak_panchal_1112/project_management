class CreateLetters < ActiveRecord::Migration[5.0]
  def change
    create_table :letters do |t|
      t.integer :organization_id
      t.text :description
      t.string :title
      t.boolean :is_default, default: false
      t.timestamps
    end
    add_index :letters, :organization_id
  end
end
