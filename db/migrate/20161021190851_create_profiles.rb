class CreateProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :profiles do |t|
      t.integer :user_id
      t.string :phone_number
      t.string :mobile_number
      t.string :home_number
      t.string :address
      t.string :city
      t.string :state
      t.string :zipcode
      t.string :country
      t.text :about_me
      t.string :prev_company_name
      t.date :joining_date
      t.date :birth_date
      t.date :marriage_date
      t.date :resign_date
      t.float :prev_company_exp
      t.string :website
      t.attachment :avatar
      t.string :designation
      t.timestamps
    end
    add_index :profiles, :user_id
  end
end
