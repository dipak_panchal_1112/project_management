class AddColumnEstimateTimeToTasks < ActiveRecord::Migration[5.0]
  def change
    add_column :tasks, :estimate_time, :string
  end
end
