class AddColumnStatusIdToOrgnizationsUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :organizations_users, :status_id, :integer, default: 1
    add_index :organizations_users, :status_id
  end
end
