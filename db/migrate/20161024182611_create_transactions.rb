class CreateTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :transactions do |t|
      t.integer :organization_id
      t.integer :subscription_id
      t.string :subscription_number
      t.float :amount, default: 0.0
      t.string :status
      t.string :response_code
      t.string :response_reason_text
      t.string :transaction_id
      t.text :response
      t.timestamps
    end
    add_index :transactions, :organization_id
    add_index :transactions, :status    
  end
end
