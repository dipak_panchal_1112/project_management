class CreateWatchers < ActiveRecord::Migration[5.0]
  def change
    create_table :watchers do |t|
      t.integer :user_id
      t.integer :task_id
      t.timestamps
    end
    add_index :watchers, :user_id
    add_index :watchers, :task_id
  end
end
