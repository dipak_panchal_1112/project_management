class CreateProfile2s < ActiveRecord::Migration[5.0]
  def change
    create_table :profile2s do |t|
      t.integer :profile_id
      t.string :company_name
      t.text :reason_for_resignation
      t.date :start_date
      t.date :end_date
      
      t.timestamps
    end
    add_index :profile2s, :profile_id
  end
end
