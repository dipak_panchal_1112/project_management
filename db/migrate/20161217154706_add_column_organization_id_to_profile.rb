class AddColumnOrganizationIdToProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :profiles, :organization_id, :integer
    add_index :profiles, :organization_id
    add_column :profiles, :organizations_user_id, :integer
    add_index :profiles, :organizations_user_id
  end
end
