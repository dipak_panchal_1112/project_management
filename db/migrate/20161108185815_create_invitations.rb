class CreateInvitations < ActiveRecord::Migration[5.0]
  def change
    create_table :invitations do |t|
      t.string :token
      t.datetime :invitation_sent_at
      t.datetime :invitation_accepted_at
      t.integer :invited_by_user_id
      t.integer :invited_by_organization_id
      t.string :invite_email
      t.integer :role_id
      t.timestamps
    end
    add_index :invitations, :token, unique: true
    add_index :invitations, :invited_by_user_id
    add_index :invitations, :invited_by_organization_id
  end
end
