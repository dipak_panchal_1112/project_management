class AddColumnSlugToOrganizations < ActiveRecord::Migration[5.0]
  def change
    add_column :organizations, :slug, :string
    add_attachment :organizations, :logo
    add_index :organizations, :slug
    add_column :organizations, :about_us, :text
  end
end