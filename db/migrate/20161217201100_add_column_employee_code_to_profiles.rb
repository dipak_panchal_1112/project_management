class AddColumnEmployeeCodeToProfiles < ActiveRecord::Migration[5.0]
  def change
    add_column :profiles, :employee_code, :string
    add_column :profiles, :gender, :integer
    add_column :profiles, :education_status, :string
    add_column :profiles, :blood_group, :string
    add_column :profiles, :personal_email, :string
    add_column :profiles, :personal_skype, :string
    add_column :profiles, :skype, :string
    add_column :profiles, :status, :boolean, default: true
    add_column :profiles, :appointment_type, :integer, default: 1

    add_column :profiles, :bank_name, :string
    add_column :profiles, :account_number, :string

    add_column :profiles, :same_as_current_address, :boolean, default: false
    add_column :profiles, :permanent_address, :string
    add_column :profiles, :permanent_city, :string
    add_column :profiles, :permanent_state, :string
    add_column :profiles, :permanent_country, :string
    add_column :profiles, :permanent_zipcode, :string

    add_column :profiles, :parent_name, :string
    add_column :profiles, :relationship_with_parent, :string
    add_column :profiles, :parent_phone_number, :string

    add_column :profiles, :parent_address, :string
    add_column :profiles, :parent_city, :string
    add_column :profiles, :parent_state, :string
    add_column :profiles, :parent_country, :string
    add_column :profiles, :parent_zipcode, :string
  end
end
