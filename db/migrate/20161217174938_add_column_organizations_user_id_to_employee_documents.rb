class AddColumnOrganizationsUserIdToEmployeeDocuments < ActiveRecord::Migration[5.0]
  def change
    add_column :employee_documents, :organizations_user_id, :integer
    add_index :employee_documents, :organizations_user_id
    remove_column :employee_documents, :organization_id    
  end
end
