class RemoveColumnStatusToProfiles < ActiveRecord::Migration[5.0]
  def change
  	remove_column :profiles, :status
  end
end
