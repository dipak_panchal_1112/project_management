class CreateHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :histories do |t|
      t.integer :task_id
      t.integer :user_id
      t.text :message
      t.string :change_type
      t.timestamps
    end
    add_index :histories, :task_id
    add_index :histories, :user_id
  end
end
