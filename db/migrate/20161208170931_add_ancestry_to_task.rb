class AddAncestryToTask < ActiveRecord::Migration[5.0]
  def change
    add_column :tasks, :ancestry, :string
    add_index :tasks, :ancestry
  end
end
