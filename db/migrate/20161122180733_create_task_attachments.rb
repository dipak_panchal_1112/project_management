class CreateTaskAttachments < ActiveRecord::Migration[5.0]
  def change
    create_table :task_attachments do |t|
      t.integer :task_id
      t.integer :user_id
      t.integer :project_id
      t.attachment :document
      t.timestamps
    end
    add_index :task_attachments, :task_id
    add_index :task_attachments, :user_id
    add_index :task_attachments, :project_id    
  end
end
