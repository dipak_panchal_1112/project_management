class CreateTasksUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :tasks_users do |t|
      t.integer :task_id
      t.integer :user_id      
      t.integer :total_min, default: 0
      t.timestamps
    end
    add_index :tasks_users, :task_id
    add_index :tasks_users, :user_id
  end
end
