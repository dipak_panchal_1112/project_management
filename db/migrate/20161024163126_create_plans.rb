class CreatePlans < ActiveRecord::Migration[5.0]
  def change
    create_table :plans do |t|
      t.string :name
      t.float :price, default: 0.0
      t.string :title
      t.integer :users, default: 0
      t.integer :storage, default: 0
      t.boolean :primary, default: false
      t.timestamps
    end
  end
end
