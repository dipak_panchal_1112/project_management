class AddColumnIfscCodeToProfiles < ActiveRecord::Migration[5.0]
  def change
    add_column :profiles, :ifsc_code, :string
  end
end
