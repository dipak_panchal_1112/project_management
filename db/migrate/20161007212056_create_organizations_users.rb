class CreateOrganizationsUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :organizations_users do |t|
      t.integer :user_id
      t.integer :organization_id
      t.integer :role_id
      t.timestamps
    end
    add_index :organizations_users, :user_id
    add_index :organizations_users, :organization_id
  end
end
