class AddColumnTitleToOrganizations < ActiveRecord::Migration[5.0]
  def change
    add_column :organizations, :title, :string
    add_column :organizations, :address, :string
    add_column :organizations, :contact_number, :string
    add_column :organizations, :email, :string
    add_column :organizations, :website, :string
    add_column :organizations, :status_id, :integer, default: 1
  end
end
