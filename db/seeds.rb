# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

## Super Admin Creation

user = User.new({ firstname: 'Dipak', lastname: 'Panchal', username: 'admin', email: Rails.application.secrets.super_admin_email, password: Rails.application.secrets.super_admin_password, password_confirmation: Rails.application.secrets.super_admin_password })
organization = Organization.find_or_create_by({name: 'admin'})
ou = user.organizations_users.build(organization_id: organization.id, role_id: 1)
ou.build_profile(organization_id: organization.id, user_id: user.id)
user.skip_confirmation!
user.save
ou.profile.update_attributes(user_id: user.id)

## Add Plans

Plan.create(name: 'free', price: 0.0, title: 'Free', users: 1, storage: 100)
Plan.create(name: 'basic', price: 9.99, title: 'Basic', users: 10, storage: 1024, primary: true)
Plan.create(name: 'standard', price: 49.99, title: 'Standard', users: 50, storage: 10240, primary: true)
Plan.create(name: 'premium', price: 99.99, title: 'Premium', users: 0, storage: 0)

