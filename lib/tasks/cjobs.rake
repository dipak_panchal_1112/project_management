namespace :cjobs do
  desc "----  1. Delete Unnecessory Files from database [ Task Attachment ] --------------"
  task remove_unnecessory_task_attachments: :environment do  ## rake cjobs:remove_unnecessory_task_attachments
  	task_attachments = TaskAttachment.where('token IS NOT NULL')
  	task_attachments.each do |attachment|
  		attachment.destroy
  	end
	end
end