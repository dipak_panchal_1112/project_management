module OrganizationDetail
	def phone_number
		owner.try(:profile).try(:phone_number)
	end

	def mobile_number
		owner.try(:profile).try(:mobile_number)
	end

	def home_number
		owner.try(:profile).try(:home_number)
	end

	def pwebsite
		owner.try(:profile).try(:website)
	end

	def to_address
		owner.try(:profile).try(:address)
	end

	def city
		owner.try(:profile).try(:city)
	end

	def state
		owner.try(:profile).try(:state)
	end

	def country
		owner.try(:profile).try(:country)
	end

	def zipcode
		owner.try(:profile).try(:zipcode)
	end

	def about_me
		owner.try(:profile).try(:about_me)
	end

	def birthdate
		if owner.try(:profile).try(:birth_date)
			owner.try(:profile).try(:birth_date).strftime('%d/%m/%Y')
		end
	end

	def marriage_date
		if owner.try(:profile).try(:marriage_date)
			owner.try(:profile).try(:marriage_date).strftime('%d/%m/%Y')
		end
	end

	def designation
		owner.try(:profile).try(:designation)
	end
end