# == Schema Information
#
# Table name: profiles
#
#  id                       :integer          not null, primary key
#  user_id                  :integer
#  phone_number             :string
#  mobile_number            :string
#  home_number              :string
#  address                  :string
#  city                     :string
#  state                    :string
#  zipcode                  :string
#  country                  :string
#  about_me                 :text
#  joining_date             :date
#  birth_date               :date
#  marriage_date            :date
#  resign_date              :date
#  website                  :string
#  avatar_file_name         :string
#  avatar_content_type      :string
#  avatar_file_size         :integer
#  avatar_updated_at        :datetime
#  designation              :string
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  organization_id          :integer
#  organizations_user_id    :integer
#  firstname                :string
#  lastname                 :string
#  employee_code            :string
#  gender                   :integer
#  education_status         :string
#  blood_group              :string
#  personal_email           :string
#  personal_skype           :string
#  skype                    :string
#  appointment_type         :integer          default(1)
#  bank_name                :string
#  account_number           :string
#  same_as_current_address  :boolean          default(FALSE)
#  permanent_address        :string
#  permanent_city           :string
#  permanent_state          :string
#  permanent_country        :string
#  permanent_zipcode        :string
#  parent_name              :string
#  relationship_with_parent :string
#  parent_phone_number      :string
#  parent_address           :string
#  parent_city              :string
#  parent_state             :string
#  parent_country           :string
#  parent_zipcode           :string
#  ifsc_code                :string
#

require 'test_helper'

class ProfileTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
