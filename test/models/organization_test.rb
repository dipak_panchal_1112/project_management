# == Schema Information
#
# Table name: organizations
#
#  id                    :integer          not null, primary key
#  name                  :string           default(""), not null
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  deleted_at            :datetime
#  title                 :string
#  address               :string
#  contact_number        :string
#  email                 :string
#  website               :string
#  status_id             :integer          default(1)
#  slug                  :string
#  logo_file_name        :string
#  logo_content_type     :string
#  logo_file_size        :integer
#  logo_updated_at       :datetime
#  about_us              :text
#  plan_id               :integer
#  payment_status        :string
#  subscription_id       :integer
#  track_timing_for_task :boolean          default(FALSE)
#

require 'test_helper'

class OrganizationTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
