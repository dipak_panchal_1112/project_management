# == Schema Information
#
# Table name: tasks
#
#  id            :integer          not null, primary key
#  slug          :string
#  project_id    :integer
#  issue_type_id :integer          default(3)
#  summary       :text
#  description   :text
#  due_date      :date
#  status_id     :integer          default(1)
#  priority      :integer          default(1)
#  resolution    :integer
#  reporter_id   :integer
#  creator_id    :integer
#  total_min     :integer          default(0)
#  task_number   :integer          default(0)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  estimate_time :string
#  ancestry      :string
#

require 'test_helper'

class TaskTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
