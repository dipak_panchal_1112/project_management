module ApplicationHelper

	def is_super_admin?
		@current_role == 'super_admin'
	end

	def is_hr_access?
		['hr_manager', 'organization_owner'].include?(@current_role)
	end

	def is_hr_manager?
		@current_role == 'hr_manager'
	end

	def is_org_owner?
		@current_role == 'organization_owner'
	end

	def current_user_profile
		current_user.organizations_users.find_by(organization_id: @current_organization).profile		
	end
end
