module EmployeesHelper
	def memeber_since(created)
		"Member since - #{created}".html_safe
	end

	def document_attachment_type2(doc)
		case doc.document_content_type
		when 'image/jpeg', 'image/png'
			html = "<div class='image'>								
	            	<img alt='image' class='img-responsive' src='#{doc.document.url}'>
	          	</div>".html_safe
		else
			html = "<div class='icon'><i class='fa fa-file'></i></div>"
		end
		html.html_safe
	end

  def role_for_employee(user_organization)
    role  = user_organization.my_role.gsub('_', ' ').titleize
    return role.length <= 4 ? role.upcase : role
  end

end
