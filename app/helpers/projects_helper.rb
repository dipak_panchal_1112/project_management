module ProjectsHelper
	def project_created_at(created_at)
		html = ''
		html += "<small>Created #{created_at.strftime('%d.%m.%Y') }</small>"
		html.html_safe
	end

	def project_status(project)
		html = ''
		case project.status_id
		when 1
			html += "<span class='label label-plain'>#{project.status}</span>"
		when 2
			html += "<span class='label label-primary'>#{project.status}</span>"
		when 3
			html += "<span class='label label-success'>#{project.status}</span>"
		when 4
			html += "<span class='label label-warning'>#{project.status}</span>"
		when 5
			html += "<span class='label label-danger'>#{project.status}</span>"
		end
		html.html_safe
	end
end
