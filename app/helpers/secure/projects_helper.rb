module Secure::ProjectsHelper
	def task_code(status_id, code)
		case status_id
		when 1
			html = "<span class='label label-danger'>#{code}</span>"
		when 2
			html = "<span class='label label-danger2'>#{code}</span>"
		when 3
			html = "<span class='label label-warning'>#{code}</span>"
		when 4
			html = "<span class='label label-primary'>#{code}</span>"
		when 5		
			html = "<span class='label label-plain'>#{code}</span>"
		end
		html.html_safe
	end	
end
