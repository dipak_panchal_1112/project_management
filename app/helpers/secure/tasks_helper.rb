module Secure::TasksHelper
	def task_type_with_icon(task)
		case task.issue_type_id
		when 1
			html = "<label class='control-label'><img src='/assets/task-type/bug.svg' class='m-r-3'/>#{task.issue_type}</label>"
		when 2
			html = "<label class='control-label'><img src='/assets/task-type/change.png' class='m-r-3'/>#{task.issue_type}</label>"
		when 3
			html = "<label class='control-label'><img src='/assets/task-type/task.svg' class='m-r-3'/>#{task.issue_type}</label>"
		when 4
			html = "<label class='control-label'><img src='/assets/task-type/epic.svg' class='m-r-3'/>#{task.issue_type}</label>"
		when 5, 6
			html = "<label class='control-label'><img src='/assets/task-type/story.svg' class='m-r-3'/>#{task.issue_type}</label>"
		end
		html = html.html_safe		
	end

	def show_task_status(task)
		html = "<label class='control-label'>"
		case task.status_id
		when 1 ## To Do
			sub_html = "<span class='label label-default'>#{task.status.upcase}</span>"
		when 2 ## In Process
			sub_html = "<span class='label label-warning'>#{task.status.upcase}</span>"
		when 3 ## Cancelled
			sub_html = "<span class='label label-danger'>#{task.status.upcase}</span>"
		when 4 ## Review
			sub_html = "<span class='label label-information'>#{task.status.upcase}</span>"
		when 5 ## Testing
			sub_html = "<span class='label label-default'>#{task.status.upcase}</span>"
		when 6 ## Done
			sub_html = "<span class='label label-success'>#{task.status.upcase}</span>"
		when 7 ## Reopen
			sub_html = "<span class='label label-default'>#{task.status.upcase}</span>"
		end
		html = (html + sub_html + '</label>').html_safe
	end

	def task_priority_icon(priority)
		html = "<i class='fa fa-arrow-circle-up #{priority.downcase}' title='#{priority}'></i>".html_safe		
	end

	def attachment_thumbnail_display(attachment)
		if ['image/jpeg', 'image/gif', 'image/png'].include?(attachment.document_content_type)
			html = "<div class='image'>
								<img alt='Attachment' class='img-responsive' src='#{attachment.document.url}'>
							</div>"
		else
			html = "<div class='icon'><i class='fa fa-file'></i></div>"
		end
		html.html_safe
	end

end
