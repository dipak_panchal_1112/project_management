module AdminHelper
	def organization_created_at(organization)
		html = ""
		html += "<small>Created #{organization.created_at.strftime('%d/%m/%Y')}</small>"
		return html.html_safe
	end

	def loader
		html = ""
		html += "<i class='fa fa-spin fa-spinner'></i> Loading..."
		return html.html_safe
	end

	def wait_loader
		html = ""
		html += "<i class='fa fa-spin fa-spinner'></i> Wait..."
		return html.html_safe
	end

	def icon_loader
		html = ""
		html += "<i class='fa fa-spin fa-spinner'></i>"
		return html.html_safe
	end

	def attachment_loader
		html = ""
		html += "<i class='fa fa-spin fa-spinner attachment-download'></i><span class='atdown'>DOWNLOADING</span>"
		return html.html_safe		
	end

	def show_breadcrumb(title)
    html = ""
    html += "<h2>#{title}</h2>"
    html += "<ol class='breadcrumb'>#{render_breadcrumbs}</ol>"
    return html.html_safe
	end

	def organization_status(status)
		if status.downcase == 'active' 
			return "<span class='label label-primary'>#{status.humanize}</span>".html_safe
		else
			return "<span class='label label-danger'>#{status.humanize}</span>".html_safe
		end
	end

	def image_show(image_url)
		if image_url == 'No picture'
			'/assets/not-found.png'
		else
			return image_url
		end
	end

	def transaction_status(status)
		html = "<span class='label label-primary'>#{status.humanize}</span>"
		return html.html_safe
	end

	def profile_short_name(org, user)
		profile = Profile.find_by(organization_id: org.id, user_id: user.id)
		return profile.short_name.upcase
	end
end
