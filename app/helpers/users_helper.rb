module UsersHelper
	## Flash Message
	def bootstrap_icon_for flash_type
    { 
    	success: 'check-circle',
    	error: 'remove',
    	alert: 'warning',
    	notice: 'exclamation-circle'
    }[flash_type] || 'question-circle'
  end

	def flash_class(level)
    case level
      when 'notice' then 'alert-info'
      when 'success' then 'alert-success'
      when 'error' then 'alert-danger'
      when 'alert' then 'alert-warning'
    end
	end

  def my_current_role(role)
    role.gsub('_', ' ').titleize
  end

  def role_for_current_organization(user, organization)
    org   = user.organizations_users.find_by(organization_id: organization.id)    
    role  = org.my_role.gsub('_', ' ').titleize
    return role.length <= 4 ? role.upcase : role
  end

  def status_of_user_for_current_organization(user, organization)
    org   = user.organizations_users.find_by(organization_id: organization.id)    
    return user_status(org.status_id)
  end

  def is_organization_owner?(user,organization)
    role = role_for_current_organization(user, organization)
    role == 'Organization Owner'
  end

  def user_status(status_id)
    html = ''
    role = Rails.application.secrets.statuses[status_id].humanize
    case status_id
    when 1
      html += "<span class='label label-primary'>#{role}</span>"
    when 2
      html += "<span class='label label-info'>#{role}</span>"
    when 3
      html += "<span class='label label-danger'>#{role}</span>"
    when 4
      html += "<span class='label label-warning'>#{role}</span>"
    else
      html += "<span class='label label-primary'>Unknown</span>"
    end
    html.html_safe
  end
end