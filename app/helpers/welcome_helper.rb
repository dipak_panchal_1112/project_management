module WelcomeHelper
	def plan_name(name)
		name.humanize
	end

	def plan_price(price)
		price > 1 ? "$ #{price} / Month" : "$ #{price}"
	end

	def plan_department(users)
		users == 0 ? "Unlimited Users / Department" : "#{pluralize(users, 'User')} / Department"
	end

	def plan_storage(storage)
		if storage == 0
			'Unlimited Storage'
		elsif storage < 1000			
			"#{storage} MB Storage"
		else
			"#{(storage / 1024).to_i} GB Storage"
		end		
	end

end
