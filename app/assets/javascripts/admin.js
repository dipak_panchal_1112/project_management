// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require dashboard/profile.js
//= require admin/plans.js

// Delete Request
$(function() {
  return $("[data-behavior='delete']").on("click", function(e) {  	
    return swal({
      title: 'Are you sure want to delete?',
      text: 'You will not be able to recover this data!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'Cancel',
      closeOnConfirm: false,
      closeOnCancel: false,
    }, (function(_this) {
      return function(confirmed) {
        if (confirmed) {
          $.ajax({
            url: $(_this).data("href"),
            dataType: "JSON",
            method: "DELETE",
            success: function(data) {
            	if(data.total == 0){
            		swal('Deleted!', 'Your data has been deleted.', 'success');
            		$('#data-'+ data.id).parent().html("<h3 class='text-center'> No Data Found ! </h3>")
            	}else{
  	          	$('#data-'+ data.id).remove();
	              swal('Deleted!', 'Your data has been deleted.', 'success');
            	}
            }
          });
        }else {
          swal("Cancelled", "Your data is safe :)", "error");
        }
      };
    })(this));
  });
});

$(function() {
  return $("[data-behavior='org-delete']").on("click", function(e) {    
    return swal({
      title: 'Are you sure want to delete?',
      text: 'You will not be able to recover this data!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'Cancel',
      closeOnConfirm: false,
      closeOnCancel: false,
    }, (function(_this) {
      return function(confirmed) {
        if (confirmed) {
          $.ajax({
            url: $(_this).data("href"),
            dataType: "JSON",
            method: "DELETE",
            success: function(data) {
              if(data.total == 0){
                swal('Deleted!', 'Your data has been deleted.', 'success');
                $('#data-'+ data.id).parent().html("<h3 class='text-center'> No Data Found ! </h3>")
              }else{
                $('#data-'+ data.id).remove();
                swal('Deleted!', 'Your data has been deleted.', 'success');
              }
            }
          });
        }else {
          swal("Cancelled", "Your data is safe :)", "error");
        }
      };
    })(this));
  });
});
