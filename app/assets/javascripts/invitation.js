// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
// Custom and plugin javascript

//= require metisMenu/jquery.metisMenu.js
//= require slimscroll/jquery.slimscroll.min.js
//= require pace/pace.min.js
//= require other/inspinia.js

// Nested Form and Validation
//= require jquery_nested_form
//= require jquery.validate
//= require jquery.validate.additional-methods

// Sparkline
//= require sparkline/jquery.sparkline.min.js
//= require demo/sparkline-demo.js



$(document).on('ready page:load', function (){

	$.validator.addMethod("noSpace", function(value, element) { 
  	return value.indexOf(" ") < 0 && value != ""; 
	}, "No space please and don't leave it empty");

	$.validator.addMethod('lowercasesymbols', function(value) {
  	return value.match(/^[^A-Z]+$/);
	}, 'You must use only lowercase letters only');

	$('.user-invitation').validate({
		rules: {
			'user[username]': {
				required: true,
				noSpace: true,
				remote: {
					type: 'get',
					url: '/users/getUser',
					dataType: 'json'
				}
			},
			'user[firstname]': {
				required: true
			},
			'user[lastname]': {
				required: true
			},			
			'user[password]': {
				required: true,
				minlength: 8,
				maxlength: 20
			},
			'user[password_confirmation]': {
				required: true,
				equalTo: '#user_password',
				minlength: 8,
				maxlength: 20
			}
		},
		messages: {
			'user[username]': {
				required: "Username can't be blank !",
				remote: 'Username already exist!'
			},
			'user[firstname]': {
				required: "Firstname can't be blank !"
			},
			'user[lastname]': {
				required: "Lastname can't be blank !"
			},			
			'user[password]': {
				required: "Password can't be blank !"
			},
			'user[password_confirmation]': {
				required: "Confirmation can't be blank !",
				equalTo: 'Mismatch password !'
			}
		},
	  errorPlacement: function(error, element) {
	    if (element.attr("type") == 'checkbox') {
	      error.insertAfter('.checkbox-inline');
	    } else {
	      error.insertAfter(element);
	    }
	  }		
	})

});