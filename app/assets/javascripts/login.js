// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require other/bootstrap.min.js
//= require jquery_nested_form
//= require jquery.validate
//= require jquery.validate.additional-methods
//= require_self

// ------------------------- REGISTRATION ----------------------------

(function($, window) {
  $.fn.replaceOptions = function(options) {
    var self, $option;

    this.empty();
    self = this;

    $.each(options, function(index, option) {
      $option = $("<option></option>")
        .attr("value", option.value)
        .text(option.text);
      self.append($option);
    });
  };
})($, window);


$(document).on('turbolinks:load', function() {

	$.validator.addMethod("noSpace", function(value, element) { 
  	return value.indexOf(" ") < 0 && value != ""; 
	}, "No space please and don't leave it empty");

	$.validator.addMethod('lowercasesymbols', function(value) {
  	return value.match(/^[^A-Z]+$/);
	}, 'You must use only lowercase letters only');

	$('#new_free_registration').validate({
		rules: {
			'user[organization_name]': {
				required: true,
				noSpace: true,
				lowercasesymbols: true,
				remote: {
					type: 'get',
					url: '/organizations/getOrganization',
					dataType: 'json'
				}
			},
			'user[username]': {
				required: true,
				noSpace: true,
				remote: {
					type: 'get',
					url: '/users/getUser',
					dataType: 'json'
				}
			},
			'user[email]': {
				required: true,
				email: true,
				remote: {
					type: 'get',
					url: '/users/getUser',
					dataType: 'json'
				}				
			},
			'user[password]': {
				required: true,
				minlength: 8,
				maxlength: 20
			},
			'user[password_confirmation]': {
				required: true,
				equalTo: '#user_password',
				minlength: 8,
				maxlength: 20
			}
		},
		messages: {
			'user[organization_name]': {
				required: "Organization can't be blank !",
				remote: 'Organization name already exist !'
			},
			'user[username]': {
				required: "Username can't be blank !",
				remote: 'Username already exist!'
			},
			'user[email]': {
				required: "Email can't be blank !",
				remote: 'Email already exist!'
			},
			'user[password]': {
				required: "Password can't be blank !"
			},
			'user[password_confirmation]': {
				required: "Password Confirmation can't be blank !",
				equalTo: 'Mismatch password !'
			}
		},
	  errorPlacement: function(error, element) {
	    if (element.attr("type") == 'checkbox') {
	      error.insertAfter('.checkbox-inline');
	    } else {
	      error.insertAfter(element);
	    }
	  }		
	})

	$('.paid_registration').validate({
		rules: {
			'user[organization_name]': {
				required: true,
				noSpace: true,
				lowercasesymbols: true,
				remote: {
					type: 'get',
					url: '/organizations/getOrganization',
					dataType: 'json'
				}
			},
			'user[username]': {
				required: true,
				noSpace: true,
				remote: {
					type: 'get',
					url: '/users/getUser',
					dataType: 'json'
				}
			},
			'user[email]': {
				required: true,
				email: true,
				remote: {
					type: 'get',
					url: '/users/getUser',
					dataType: 'json'
				}				
			},
			'user[password]': {
				required: true,
				minlength: 8,
				maxlength: 20
			},
			'user[password_confirmation]': {
				required: true,
				equalTo: '#user_password',
				minlength: 8,
				maxlength: 20
			},
			'user[credit_card]': {
				required: true,
				creditcard: true,
				number: true
			},
			'user[csc]':{
				required: true,
				minlength: 3,
				maxlength: 4,
				number: true				
			},
			'user[card_holder_name]':{
				required: true
			}
		},
		messages: {
			'user[organization_name]': {
				required: "Organization can't be blank !",
				remote: 'Organization name already exist !'
			},
			'user[username]': {
				required: "Username can't be blank !",
				remote: 'Username already exist!'
			},
			'user[email]': {
				required: "Email can't be blank !",
				remote: 'Email already exist!'
			},
			'user[password]': {
				required: "Password can't be blank !"
			},
			'user[password_confirmation]': {
				required: "Password Confirmation can't be blank !",
				equalTo: 'Mismatch password !'
			},
			'user[credit_card]': {
				required: "Credit card can't be blank !",
				creditcard: "Plz enter valid card number !",
				minlength: 'Invalid credit card',
				maxlength: 'Invalid credit card',
				number: 'Plz enter valid card number !'
			},
			'user[csc]': {
				required: 'Required',
				minlength: 'Invalid',
				maxlength: 'Invalid',
				number: 'Invalid'
			},
			'user[card_holder_name]':{
				required: "Name can't be blank !"
			}
		},
	  errorPlacement: function(error, element) {
	    if (element.attr("type") == 'checkbox') {
	      error.insertAfter('.checkbox-inline');
	    } else {
	      error.insertAfter(element);
	    }
	  }		
	})


	$(document).on('change', '#user_year_expire', function(){
		var currentTime = new Date();
		current_year 		= currentTime.getFullYear();
		current_month 	= currentTime.getMonth() + 1;
		if($(this).val() == current_year){
			var options = [];
			for (i = current_month; i <= 12; i++) {
				options.push({text: i, value: i})
			}
		}else{
			var options = [
				{text: '1', value: 1},
				{text: '2', value: 2},
				{text: '3', value: 3},
				{text: '4', value: 4},
				{text: '5', value: 5},
				{text: '6', value: 6},
				{text: '7', value: 7},
				{text: '8', value: 8},
				{text: '9', value: 9},
				{text: '10', value: 10},
				{text: '11', value: 11},
				{text: '12', value: 12},
			]
		}
		$("#user_month_expire").replaceOptions(options);
	})
	// -------------------------  FORGET PASSWORD  ----------------------------
	$('#forget_password_inst').validate({
		rules: {
			'user[email]': {
				required: true,
				email: true,
				remote: {
					type: 'get',
					url: '/users/existUser',
					dataType: 'json'
				}								
			}
		},
		messages: {
			'user[email]': {
				required: "Email can't be blank !",
				remote: 'Email not exist!'
			}
		}		
	})
	// -------------------  RESEND CONFIRMATION TOKEN  ----------------------
	$('#resend_confirmation_inst').validate({
		rules: {
			'user[email]': {
				required: true,
				email: true,
				remote: {
					type: 'get',
					url: '/users/existUser',
					dataType: 'json'
				}				
			}
		},
		messages: {
			'user[email]': {
				required: "Email can't be blank !",
				remote: 'Email not exist!'
			}
		}
	})

	// -------------------------  RESET PASSWORD  ----------------------------
	$('#changed_password').validate({
		rules: {
			'user[password]': {
				required: true,
				minlength: 8,
				maxlength: 20
			},
			'user[password_confirmation]': {
				required: true,
				equalTo: '#user_password',
				minlength: 8,
				maxlength: 20
			}
		},
		messages: {
			'user[password]': {
				required: "Password can't be blank !"
			},
			'user[password_confirmation]': {
				required: "Password Confirmation can't be blank !",
				equalTo: 'Mismatch password !'
			}
		}
	});

	// ----------------------  RESEND UNLOCK INSTRUCTION  -------------------------
	$('#resent_unlock_instruction').validate({
		rules: {
			'user[email]': {
				required: true,
				email: true,
				remote: {
					type: 'get',
					url: '/users/existUser',
					dataType: 'json'
				},
				remote: {
					type: 'get',
					url: '/users/lockUser',
					dataType: 'json'
				}				
			}
		},
		messages: {
			'user[email]': {
				required: "Email can't be blank !",
				remote: 'Email not exist!',
				remote: 'Email is not locked!'
			}
		}
	})
});