$(document).on('ready page:load', function (){

	$('#new_holiday').validate({
		rules: {
			'holiday[name]': {
				required: true,
			},
			'holiday[hdate]': {
				required: true,
				remote: {
					type: 'get',
					url: '/holidays/getExistHoliday',					
					dataType: 'json'
				}				
			},
			'holiday[htype]': {
				required: true
			}
		},
		messages: {
			'holiday[name]': {
				required: "Name can't be blank!"				
			},
			'holiday[hdate]': {
				required: "Date can't be blank!",
				remote: 'Already added for this date!'
			},
			'holiday[htype]': {
				required: "Type can't be blank!"
			}
		}
	})

  $('#holiday-calendar').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'Title',
      right: ''
    },
    editable: false,
    events: gon.holiday_list
  });



});

$(function() {
  return $("[data-behavior='holiday-delete']").on('click', function(e) {
    return swal({
      title: 'Are you sure want to delete?',
      text: 'You will not be able to recover this data!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'Cancel',
      closeOnConfirm: false,
      closeOnCancel: false
    }, (function(_this) {
      return function(confirmed) {
        if (confirmed) {
          $.ajax({
            url: $(_this).data('href'),
            dataType: 'JSON',
            method: 'DELETE',
            success: function(data) {
              if (data.total === 0) {
                swal('Deleted!', 'Your data has been deleted.', 'success');
                if(data.remove == true){
                  $('.margins-rmv').remove();
                }else{
                  $('#' + data.id).parent().parent().parent().html("<p> No Holiday Found </p>");
                }                
              } else {
                $('#' + data.id).remove();
                swal('Deleted!', 'Your data has been deleted.', 'success');
              }
            }
          });
        } else {
          swal('Cancelled', 'Your data is safe :)', 'error');
        }
      };
    })(this));
  });
});
