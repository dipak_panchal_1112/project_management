$(document).on('ready page:load', function () {

	$.validator.addMethod("noSpace", function(value, element) { 
  	return value.indexOf(" ") < 0 && value != ""; 
	}, "No space please and don't leave it empty");

	$.validator.addMethod('lowercasesymbols', function(value) {
  	return value.match(/^[^A-Z]+$/);
	}, 'You must use only lowercase letters only');

	$('.plan-edit-new').validate({
		rules: {
			'plan[name]': {
				required: true,
				noSpace: true,
				minlength: 4,
				maxlength: 10,
				remote: {
					type: 'get',
					url: '/admin/plans/getPlan',
					dataType: 'json'
				}				
			},
			'plan[title]': { required: true },
			'plan[price]': {
				required: true,
				number: true
			},
			'plan[users]': {
				required: true,
				number: true
			},
			'plan[storage]': {
				required: true,
				number: true
			}
		},
		messages: {
			'plan[name]': {
				required: "Name can't be blank !",
				remote: 'Name already exist !'				
			},
			'plan[title]': {
				required: "Description can't be blank !"
			},
			'plan[price]': {
				required: "Price can't be blank !",
				number: 'Only number allow !'
			},
			'plan[users]': {
				required: 'Required !',
				number: 'Invalid !'
			},
			'plan[storage]':{
				required: "Can't be blank!",
				number: 'Only number allowed !'

			}
		},
	  errorPlacement: function(error, element) {
	    if (element.attr("type") == 'checkbox') {
	      error.insertAfter('.checkbox-inline');
	    } else {
	      error.insertAfter(element);
	    }
	  }		
	})

	// 2nd Javascript
});
