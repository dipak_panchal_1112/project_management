$(document).on('ready page:load', function (){
	$('.edit-project-detail').validate({
		
		rules: {
			'project[name]': {
				required: true,
				remote: {
					type: 'get',
					url: '/projects/getProNameUniq',
					data: {project_id: gon.project_id},
					dataType: 'json'
				}
			},
			'project[description]': {
				required: true
			}
		},
		messages: {
			'project[name]': {
				required: "Project name can't be blank !",
				remote: 'Already exist please you another !'
			},
			'project[description]': {
				required: "Description can't be blank !"
			}		
		}

	})

	// Client Detail Page Validation

	$('.edit-project-clients').validate();

	// Project Invitation
	$('.project-invitation').validate({
		rules: {
			'project[user_ids]': {
				required: true
			}
		},
		messages: {
			'project[user_ids]': {
				required: "Can't be blank !"
			}
		}
	})

	// On change of user - we will display basic user information
	$(document).on('change', '#project_user_ids', function(){
		user_id = $(this).val()
		if(user_id != ''){
			$.getJSON('/users/'+ user_id, function(data) {
				$('.view-user-info-for-invitation').removeClass('hide')
	    	$('.view-user-info-for-invitation p img').attr('src', data['image'])
	    	$('.view-user-info-for-invitation h4').html(data['fullname'])
	    	$('.view-user-info-for-invitation h5').html(data['designation'])
			});
		}else{
			$('.view-user-info-for-invitation').addClass('hide')
    	$('.view-user-info-for-invitation p img').attr('src', '')
    	$('.view-user-info-for-invitation h4').html('')
    	$('.view-user-info-for-invitation h5').html('')			
		}
	})

})	