// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
// Custom and plugin javascript

//= require metisMenu/jquery.metisMenu.js
//= require slimscroll/jquery.slimscroll.min.js
//= require pace/pace.min.js
//= require other/inspinia.js
//= require jquery.remotipart

// Nested Form and Validation
//= require jquery_nested_form
//= require jquery.validate
//= require jquery.validate.additional-methods

// I Check
//= require iCheck/icheck.min.js

// Jvectormap
//= require jvectormap/jquery-jvectormap-2.0.2.min.js
//= require jvectormap/jquery-jvectormap-world-mill-en.js

// Sparkline
//= require sparkline/jquery.sparkline.min.js
//= require demo/sparkline-demo.js

// Toastr
//= require toastr

// File Upload
//= require jasny/jasny-bootstrap.min.js
//= require dropzone/dropzone.js
//= require codemirror/codemirror.js


// Profile Page
//= require dashboard/profile.js

// Sweat Alert
//= require sweetalert/sweetalert.min.js

// Jquery Mask Inputs
//= require other/jquery.maskedinput.js

// Date Picker
//= require datapicker/bootstrap-datepicker.js

// Summernotes
//= require summernote/summernote.min.js

// Multi Select Dropdown
//= require bootstrap-multiselect

// Inline Edit
//= require bootstrap-editable
//= require bootstrap-editable-rails

// Toastr
//= require toastr

// Clokcpicker
//= require bootstrap/clockpicker.min

// custom
//= require secure/tasks.js
//= require secure/comments.js
//= require secure/agile_boards.js
//= require employee.js

// Jquery Timer
// require other/jquery.timer.js
// require other/timerDemo.js

// Charts
//= require flot/jquery.flot.js
//= require flot/jquery.flot.tooltip.min.js
//= require flot/jquery.flot.resize.js
//= require flot/jquery.flot.pie.js
//= require flot/jquery.flot.time.js


// Calendar
//= require moment 
//= require fullcalendar

// Phone Number Mask Input

// Dropzone File Upload - Task Edit Page

// Custom
//= require hr/holidays.js
//= require secure/projects.js


  // Dropzone.autoDiscover = false;
  // var myDropzone = new Dropzone("#dropzoneForm", {
  // //Dropzone.options.dropzoneForm = {
  //   paramName: "file", // The name that will be used to transfer the file
  //   maxFilesize: 10, // MB
  //   dictDefaultMessage: "<strong>Drop files here or click to upload. </strong>",  
  // });

  toastr.options = {
    "closeButton": false,
    "debug": false,
    "progressBar": false,
    "preventDuplicates": false,
    "positionClass": "toast-top-right",
    "onclick": null,
    "showDuration": "400",
    "hideDuration": "1000",
    "timeOut": "7000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }

$(document).on('ready page:load', function (){
	$(".phone-number").mask("(999) 999-9999");
	$(".home-number").mask("(99) 999-999-99");

  
  $('.clockpicker').clockpicker({
    donetext: 'Done'
  });

  $.fn.editable.defaults.mode = 'inline'

  // Inline Form Validation for task  
  $(".in-task-summary-et").editable({ 
    validate: function(value){
      if($.trim(value) == ''){
        return "Can't be blank !"
      }
    },
    success: function(response, newValue){
      if(response.status == 200){
        $('.project-task-summary-'+ gon.task_slug).html(newValue)
      }
    }    
  })

  $('.in_task_reporter_edt').editable({
    source: gon.reporter_list,    
    validate: function(value){
      if($.trim(value) == ''){
        return "Can't be blank !"
      }
    }    
  })

  $('.in_task_assignees_edt').editable({
    source: gon.reporter_list,
    validate: function(value){
      if($.trim(value) == ''){
        return "Can't be blank !"
      }      
    }
  })

  // Estimate Time - Inline Edit
  $('.in_task_estimate_time_edt').editable();

});

$('.date').datepicker({
  todayBtn: "linked",
  keyboardNavigation: false,
  forceParse: false,
  calendarWeeks: true,
  autoclose: true,
  format: "dd/mm/yyyy"
});

$('.date-old').datepicker({
  autoclose: true,
  endDate: '+10d',
  daysOfWeekDisabled: '0',
  format: "dd/mm/yyyy"
});

var dateToday = new Date();
$('.holiday-date').datepicker({
  autoclose: true,  
  startDate: '-1d',
  daysOfWeekDisabled: '0',
  format: "dd/mm/yyyy"
})

$(".chosen-select").chosen({
    no_results_text: 'No results matched',
    width: '100%'
});

$(document).on('nested:fieldAdded', function(event){
  $('.date-old').datepicker({
    autoclose: true,
    endDate: '+10d',
    daysOfWeekDisabled: '0',
    format: "dd/mm/yyyy"
  });  
});