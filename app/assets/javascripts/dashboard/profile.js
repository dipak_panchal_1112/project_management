$(document).on('ready page:load', function (event) {
	$("#sparkline1").sparkline([34, 43, 43, 35, 44, 32, 44, 48], {
  	type: 'line',
  	width: '100%',
  	height: '50',
  	lineColor: '#1ab394',
  	fillColor: "transparent"
  });
});

// -------------------- NEW JAVASCRIPT START HERE ----------------------

// ------------------------- VALIDATION OF PROFILE PAGE  -----------------------------------

//-------- VALIDATION OF CHANGE PASSWORD ------------------------

$(document).on('ready page:load', function () {

	$.validator.addMethod("noSpace", function(value, element) { 
  	return value.indexOf(" ") < 0 && value != ""; 
	}, "No space please and don't leave it empty");

	$.validator.addMethod('lowercasesymbols', function(value) {
  	return value.match(/^[^A-Z]+$/);
	}, 'You must use only lowercase letters only');

	$('#edit_user').validate({
		rules: {
			'user[current_password]': {
				required: true,
				noSpace: true,
				minlength: 8,
				maxlength: 20,
				remote: {
					type: 'get',
					url: '/users/getPassword',
					dataType: 'json'
				}				
			},
			'user[password]': {
				required: true,
				minlength: 8,
				maxlength: 20
			},
			'user[password_confirmation]': {
				required: true,
				equalTo: '#user_password',
				minlength: 8,
				maxlength: 20
			}
		},
		messages: {
			'user[current_password]': {
				required: "Current password can't be blank !",
				remote: 'Current password is invalid !'				
			},
			'user[password]': {
				required: "Password can't be blank !"
			},
			'user[password_confirmation]': {
				required: "Password Confirmation can't be blank !",
				equalTo: 'Mismatch password !'
			}
		},
	  errorPlacement: function(error, element) {
	    if (element.attr("type") == 'checkbox') {
	      error.insertAfter('.checkbox-inline');
	    } else {
	      error.insertAfter(element);
	    }
	  }		
	})

	$("#profile-edit-user").validate({
		rules: {
			'profile[current_password]': {
				required: true,
				noSpace: true,
				minlength: 8,
				maxlength: 20,
				remote: {
					type: 'get',
					url: '/users/getPassword',
					dataType: 'json'
				}				
			}
		},
		messages: {
			'profile[current_password]': {
				required: "Current password can't be blank !",
				remote: 'Current password is invalid !'				
			}
		},
	  errorPlacement: function(error, element) {
	    if (element.attr("type") == 'checkbox') {
	      error.insertAfter('.checkbox-inline');
	    } else {
	      error.insertAfter(element);
	    }
	  }			
	})
});
