// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
// Custom and plugin javascript

//= require metisMenu/jquery.metisMenu.js
//= require slimscroll/jquery.slimscroll.min.js
//= require pace/pace.min.js
//= require other/inspinia.js
//= require jquery.remotipart

// Nested Form and Validation
//= require jquery_nested_form
//= require jquery.validate
//= require jquery.validate.additional-methods

// I Check
//= require iCheck/icheck.min.js

// Jvectormap
//= require jvectormap/jquery-jvectormap-2.0.2.min.js
//= require jvectormap/jquery-jvectormap-world-mill-en.js

// Sparkline
//= require sparkline/jquery.sparkline.min.js
//= require demo/sparkline-demo.js

// Toastr
//= require toastr

// Profile Page
//= require dashboard/profile.js

// File Upload
//= require jasny/jasny-bootstrap.min.js
//= require dropzone/dropzone.js
//= require codemirror/codemirror.js

// Sweat Alert
//= require sweetalert/sweetalert.min.js

// Jquery Mask Inputs
//= require other/jquery.maskedinput.js

// Date Picker
//= require datapicker/bootstrap-datepicker.js

// Phone Number Mask Input

// Custom
//= require hr/holidays.js
//= require employee.js

// Calendar
//= require moment 
//= require fullcalendar

$(document).on('ready page:load', function (){
	$(".phone-number").mask("(999) 999-9999");
	$(".home-number").mask("(99) 999-999-99");
});

$('.date').datepicker({
  todayBtn: "linked",
  keyboardNavigation: false,
  forceParse: false,
  calendarWeeks: true,
  autoclose: true,
  format: "dd/mm/yyyy"
});

var dateToday = new Date();
$('.holiday-date').datepicker({
  autoclose: true,  
  startDate: '-1d',
  daysOfWeekDisabled: '0',
  format: "dd/mm/yyyy"
})

$(".chosen-select").chosen({
    no_results_text: 'No results matched',
    width: '100%'
});