$(document).on('ready page:load', function (){
	$('.add-new-letter').validate({
		
		rules: {
			'letter[title]': {
				required: true
			}
		},
		messages: {
			'letter[title]': {
				required: "Title can't be blank !",				
			}
		}
	})
});