// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require invitations.js
// Wyswyg
//= require summernote/summernote.min.js

// Validations
//= require projects.js
//= require letters.js


$(document).on('ready page:load', function (){
	$('.summernote').summernote({
		toolbar: [
			['style', ['bold', 'italic', 'underline', 'fontname', 'hr']],	    
	    ['fontsize', ['color', 'fontsize', 'ul', 'ol', 'paragraph', 'height', 'help']],	    
		]
	});

	$('.summernote-letter').summernote({		
		placeholder: 'Write your letter here...',
		toolbar: [
		  ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
	    ['font', ['strikethrough', 'superscript', 'subscript']],
	    ['fontsize', ['fontname', 'fontsize']],
	    ['color', ['color']],
	    ['para', ['ul', 'ol', 'paragraph', 'hr', 'link']],
	    ['height', ['height']],
	    ['table', ['table']],
	    ['help', ['help']]	    
		]
	});

});
