//Flot Pie Chart
$(function() {
  if(gon.pieChartReportForProject != undefined && gon.pieChartReportForProject.length >= 1){
    var data = gon.pieChartReportForProject;
    var plotObj = $.plot($("#project-flot-pie-chart"), data, {
      series: {
        pie: { show: true }
      },
      grid: {
        hoverable: true
      },
      tooltip: true,
      tooltipOpts: {
        content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
        shifts: {
          x: 20,
          y: 0
        },
        defaultTheme: false
      }
    });    
  }
});

$(function() {
  if(gon.report2 != undefined && gon.report2.length >= 1){
    var data = gon.report2;
    var plotObj = $.plot($("#project-2-flot-pie-chart"), data, {
      series: {
        pie: { show: true }
      },
      grid: {
        hoverable: true
      },
      tooltip: true,
      tooltipOpts: {
        content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
        shifts: {
          x: 20,
          y: 0
        },
        defaultTheme: false
      }
    });    
  }
});