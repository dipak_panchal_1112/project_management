$('.file-box').each(function() {
	animationHover(this, 'pulse');
});

$("#prev-company-profile").validate();
$('#bank-account-detail').validate();

$("#employee-document-profile").validate({
	rules: {
		'organizations_user[employee_documents_attributes][0][document_name]': {
			required: true
		},
		'organizations_user[employee_documents_attributes][0][document]': {
			required: true
		}
	},
	messages: {
		'organizations_user[employee_documents_attributes][0][document_name]': {
			required: "Can't be blank !"
		},
		'organizations_user[employee_documents_attributes][0][document]': {
			required: "Can't be blank !"
		}
	}		
})

$('#basic-employee-profile').validate({	
	rules: {
		'profile[firstname]': {
			required: true
		},
		'profile[lastname]': {
			required: true
		}
	},
	messages: {
		'profile[firstname]': {
			required: "Can't be blank !"
		},
		'profile[lastname]': {
			required: "Can't be blank !"
		}
	}
})


$(function() {
  return $("[data-behavior='emp-document-delete']").on('click', function(e) {
    return swal({
      title: 'Are you sure want to delete?',
      text: 'You will not be able to recover this data!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'Cancel',
      closeOnConfirm: false,
      closeOnCancel: false
    }, (function(_this) {
      return function(confirmed) {
        if (confirmed) {
          $.ajax({
            url: $(_this).data('href'),
            dataType: 'JSON',
            method: 'DELETE',
            success: function(data) {
              if (data.total === 0) {
                swal('Deleted!', 'Your data has been deleted.', 'success');
                $('#' + data.id).parent().html("<p> No documents uploaded yet ! </p>");
              } else {
                $('#' + data.id).remove();
                swal('Deleted!', 'Your data has been deleted.', 'success');
              }
            }
          });
        } else {
          swal('Cancelled', 'Your data is safe :)', 'error');
        }
      };
    })(this));
  });
});

$('#profile_same_as_current_address').on('ifChecked', function () { 
  address = $('#profile_address').val();
  city = $('#profile_city').val();
  state = $('#profile_state').val();
  country = $('#profile_country').val();
  zipcode = $('#profile_zipcode').val();

  $('#profile_permanent_address').val(address)
  $('#profile_permanent_city').val(city)
  $('#profile_permanent_state').val(state)
  $('#profile_permanent_country').val(country)
  $('#profile_permanent_zipcode').val(zipcode)
})

$('#profile_same_as_current_address').on('ifUnchecked', function () { 
  $('#profile_permanent_address').val('')
  $('#profile_permanent_city').val('')
  $('#profile_permanent_state').val('')
  $('#profile_permanent_country').val('')
  $('#profile_permanent_zipcode').val('')  
})