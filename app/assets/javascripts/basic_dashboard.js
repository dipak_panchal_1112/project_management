//= require jquery
//= require jquery_ujs
//= require jquery-ui/jquery-ui.min.js
//= require other/bootstrap.min.js
//= require local_time
//= require iCheck/icheck.min.js

// Choosen JS
//= require chosen-jquery

// For All checkboxes

$(document).on('ready page:load', function (){
  $('.i-checks').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
  });
});