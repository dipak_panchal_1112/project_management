class DeviseMailer < Devise::Mailer
	layout 'email'

	def confirmation_instructions(record, token, opts={})
    super
  end

  # Overrides same inside Devise::Mailer
  def reset_password_instructions(record, token, opts={})
    super
  end

  def password_change(record, token, opts={})
  	super
  end

  # Overrides same inside Devise::Mailer
  def unlock_instructions(record, token, opts={})
    super
  end
end