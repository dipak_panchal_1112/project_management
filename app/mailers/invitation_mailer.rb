class InvitationMailer < ApplicationMailer
	layout 'email'

	def send_invitation(invitation, opts={})
		@invitation = invitation
    mail(to: invitation.invite_email, subject: 'SUBJECT FOR INVITATION MAILER')
  end

end
