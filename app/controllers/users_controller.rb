## Users Controllers
class UsersController < ApplicationController
	## Authentication Methods
  before_action :authenticate_user!, except: [:getUser, :existUser, :lockUser, :creditCardValidation]
  ## check_authorization for access method for limited roles  
  before_action :check_authorization, only: [:index, :show, :edit, :update, :destroy, :create]
  layout 'dashboard', except: [:getUser, :existUser, :lockUser, :creditCardValidation]
  include ApplicationHelper
  
  ## Authentication Method
  expose :users, ->{ current_organization.organizations_users.search(params[:search]) }
  expose(:user, find_by: :slug)

  add_breadcrumb 'Home', :user_dashboard_path
  add_breadcrumb 'Users', :users_path

  def update
    if user.update(user_params)
      redirect_to users_path, notice: "Successfully update '#{user.fullname}' profile !"
    else
      redirect_to users_path, flash: {error: user.errors.full_messages.join(', ')}
    end
  end

  def show
    respond_to do |format|
      format.json {        
        org_user = current_organization.organizations_users.find_by(user_id: params[:id])        
        profile = org_user.profile
        if profile.present?
          render json: {fullname: profile.fullname, designation: profile.designation, image: profile.profile_image('small') }, status: 200
        else
          render json: {error: 'No Authorize Request'}, status: 404
        end
      }
    end
  end

	## Non Authentication Methods
  def getUser    
  	user = User.where('email = ? OR username = ?', params[:user][:email], params[:user][:username]).try(:last)
  	render json: user.present? ? false : true  	
  end

  def existUser
  	user = User.where('email = ? OR username = ?', params[:user][:email], params[:user][:username]).try(:last)
  	render json: user.present? ? true : false
  end

  def lockUser
    user = User.where('email = ? OR username = ?', params[:user][:email], params[:user][:username]).try(:last)
    render json: (user.present? && user.locked_at.present?) ? true : false    
  end

  def creditCardValidation
    if params[:user][:year_expire].present? && params[:user][:month_expire].present? &&
      params[:user][:year_expire].to_s == Time.zone.now.year.to_s && params[:user][:month_expire].to_i < Time.zone.now.month.to_i
      render json: false, status: 200
    else
      render json: true, status: 200
    end    
  end

  ## Authentication Methods (Require Authentication for below method)
  def getPassword
    # This method is used for check current password is valid or not for jquery validation.    
    if params[:profile].present?
      cpassword = params[:profile][:current_password]
    else
      cpassword = params[:user][:current_password]
    end
    render json: (current_user.valid_password?(cpassword) ? true : false)
  end

  def dashboard
  	## This is user dashboard.
    if is_super_admin?
      redirect_to admin_dashboard_path
    elsif current_role == 'organization_owner'
      render layout: 'dashboard'
    else
      render layout: 'users'
    end
  end

  def lock_screen
    orgnization_user = current_user.organizations_users.find_by(organization_id: @current_organization)
    if orgnization_user.active?
      redirect_to root_path
    else
      render layout: 'lock_screen'
    end

    
  end

  private

  def user_params
    params.require(:user).permit(:firstname, :lastname, profile_attributes: [:id, :id, :user_id, :phone_number, :mobile_number, :home_number, :address, :city, :state, :zipcode, :country, :about_me, :prev_company_name, :joining_date, :birth_date, :marriage_date, :resign_date, :prev_company_exp, :website, :avatar,  :designation], organizations_users_attributes: [:id, :status_id, :role_id])    
  end

  def check_authorization
    if ['organization_owner'].include?(current_role)
    else
      return redirect_to user_dashboard_path, flash: {error: 'Access denied.'}
    end
  end

end