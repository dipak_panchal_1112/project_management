class Users::RegistrationsController < Devise::RegistrationsController
  before_action :configure_sign_up_params, only: [:create]
  before_action :configure_account_update_params, only: [:update]

  layout :get_layout, except: [:new, :create]

  # GET /resource/sign_up
  def new
    @plan = Plan.find_by(name: params[:plan].try(:strip).try(:downcase))
    if @plan.present?
      session[:plan_id] = @plan.id
      super 
    else
      redirect_to root_path
    end
  end

  # POST /resource
  def create    
    domain = params[:user][:organization_name]
    session[:domain] = "#{domain.downcase}.#{Rails.application.secrets.base_domain.gsub(/(:\d+)$/, '')}"

    build_resource(sign_up_params)
    if resource.valid?
      send_request_for_subscription_in_auth_net(resource)
    else
      redirect_to new_user_registration_path(plan: plan.name), flash: {error: 'Error: Plz fill up registration form to authenticate !'}
    end
  end

  # GET /resource/edit
  def edit
    add_breadcrumb "Home", :admin_dashboard_path    
    add_breadcrumb "Profile"
    resource.build_profile unless resource.profile.present?
    @org_user = OrganizationsUser.find_by(user_id: resource.id, organization_id: @current_organization.id)
    @profile  = @org_user.profile.present? ? @org_user.profile : @org_user.build_profile
    if !@profile.user_id.present?
      @profile.update_attributes(user_id: resource.id, organization_id: @current_organization.id)
    end
    super
  end

  # PUT /resource
  def update    
    @org_user = OrganizationsUser.find_by(user_id: resource.id, organization_id: @current_organization.id)
    if params[:profile].present?
      if @org_user.profile.present?
        @org_user.profile.update_attributes(profile_params)
      else
        @org_user.build_profile(profile_params)
        @org_user.save
      end    
      redirect_to '/users/edit', notice: 'Profile successfully updated'
    else
      super
    end
  end

  # DELETE /resource
  def destroy
    @org_user = OrganizationsUser.find_by(user_id: resource.id, organization_id: @current_organization.id)
    @org_user.destroy
    sign_out_all_scopes
    redirect_to root_path
  end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  def cancel
    super
  end

  protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username, :organization_name])
  end

  # If you have extra params to permit, append them to the sanitizer.
  def configure_account_update_params
    devise_parameter_sanitizer.permit(:account_update, keys: [:firstname, :lastname, profile_attributes: [:id, :phone_number, :mobile_number, :home_number, :address, :city, :state, :country, :zipcode, :about_me, :prev_company_name, :joining_date, :birth_date, :marriage_date, :resign_date, :prev_company_exp, :website, :avatar, :designation] ])
  end

  # The path used after sign up.
  #def after_sign_up_path_for(resource)
  #  '/thank-you'
  #end

  # The path used after sign up for inactive accounts.
  def after_inactive_sign_up_path_for(resource)
    '/thank-you'
  end

  # def add_user_in_organization(resource=nil)
  #   if resource.present? && resource.valid?
  #     organization = Organization.create(name: params[:user][:organization_name])
  #     resource.organizations_users.create(organization_id: organization.id, role_id: 2)
  #   end
  # end

  def send_request_for_subscription_in_auth_net(resource)
    plan = Plan.find_by(id: session[:plan_id])
    if plan.name != 'free'
      transaction  = AuthorizeNet::ARB::Transaction.new(Rails.application.secrets.authorize_net_login_key, Rails.application.secrets.authorize_net_transaction_key, gateway: :sandbox)
      subscription = create_authorize_net_subscription(plan)
      response     = transaction.create(subscription)      
    
      if response.success?
        get_success_response(resource, response, plan, params[:user])
      end

      if response.success? && resource.persisted?
        response_success_and_resource_persisted(resource)
      else
        redirect_to new_user_registration_path(plan: plan.name), flash: {error: "#{response.message_code}: #{response.message_text}"}
      end
    else
      get_success_response(resource, nil, plan, params[:user])
      response_success_and_resource_persisted(resource)
    end
  end

  def response_success_and_resource_persisted(resource)
    if resource.active_for_authentication?
      set_flash_message! :notice, :signed_up
      sign_up(resource_name, resource)
      respond_with resource, location: after_sign_up_path_for(resource)        
    else
      set_flash_message! :notice, :"signed_up_but_#{resource.inactive_message}"
      expire_data_after_sign_in!
      respond_with resource, location: after_inactive_sign_up_path_for(resource)        
    end
  end

  def create_authorize_net_subscription(plan)    
    subscription = AuthorizeNet::ARB::Subscription.new(
      name: "Project Management System", length: 1, unit: :month, start_date: Date.today,
      total_occurrences: :unlimited, amount: plan.price,
      invoice_number: Time.zone.now.to_i,
      description: "Project Management System - Monthly Subscription Charge - Plan: #{plan.name} - $#{plan.price} / Month",
      credit_card: to_authorize_credit_card(params[:user]),
      billing_address: to_authorize_address(params[:user])
    )    
  end  

  def to_authorize_credit_card(user)
    AuthorizeNet::CreditCard.new(user[:credit_card], "#{user[:month_expire]}#{user[:year_expire]}", options: {card_code: user[:csc]})
  end

  def to_authorize_address(user)
    AuthorizeNet::Address.new(first_name: user[:username], last_name: user[:username])
  end

  def get_success_response(resource, response=nil, plan, params)
    resource.plan = plan
    resource.save
    yield resource if block_given?        
    
    if response.present?
      subscription = Subscription.create({
        plan_id: plan.id, next_renewal_date: (Date.today + 1.month).to_date,
        credit_card_encrypted: params[:credit_card],
        card_expire: "#{params[:month_expire]}/#{params[:year_expire]}",
        subscription_id: response.subscription_id        
      })
    end
    organization = Organization.find_by(name: params[:organization_name])
    
    session[:plan_id] = nil
    
    if response.present?
      transaction = Transaction.create({
        organization_id: organization.id, subscription_id: subscription.id,
        amount: plan.price, status: 1, response_code: response.message_code,
        response_reason_text: response.message_text        
      })
    end
  end

  private

  def get_layout    
    if current_role == 'organization_owner' || current_role == 'super_admin'
      'dashboard'
    else
      'users'
    end
  end

  def profile_params
    params.require(:profile).permit(:ifsc_code, :education_status, :status, :bank_name, :account_number, :avatar, :firstname, :lastname, :phone_number, :home_number, :mobile_number, :website, :address, :city, :country, :state, :zipcode, :about_me, :birth_date, :designation, :marriage_date, :blood_group, :appointment_type, :personal_email, :skype, :personal_skype, :same_as_current_address, :permanent_address, :permanent_city, :permanent_country, :permanent_state, :permanent_zipcode, :parent_name, :parent_phone_number, :relationship_with_parent, :parent_address, :parent_city, :parent_country, :parent_state, :parent_zipcode, :gender, :joining_date, :resign_date, profile2s_attributes: [:id, :company_name, :start_date, :end_date, :reason_for_resignation, :'_destroy'], certificates_attributes: [:id, :name, :klass, :month, :year, :'_destroy'])
  end
end
