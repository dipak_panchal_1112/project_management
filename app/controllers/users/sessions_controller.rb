class Users::SessionsController < Devise::SessionsController
# before_action :configure_sign_in_params, only: [:create]
  layout 'login', only: [:new, :create]
  # GET /resource/sign_in
  
  def new
    super
  end

  # POST /resource/sign_in
  def create
    user, message = @current_organization.present? && @current_organization.users.present? && @current_organization.users.authenticate_organization_with_email_and_password(params[:user])  
    if user.present? && !message.present? && user.is_confirmed_user?
      org_user = user.organizations_users.find_by(organization_id: @current_organization.id)
      sign_out_all_scopes
      sign_in(user)
      if org_user.status_id == 1                
        if org_user.role_id == 1
          redirect_to admin_dashboard_path, notice: 'You have been logged in successfully.'
        else
          redirect_to user_dashboard_path, notice: 'You have been logged in successfully.'      
        end
      else
        redirect_to lock_screen_users_path
      end
    elsif user.present? && !user.is_confirmed_user?
      redirect_to new_user_session_path, flash: {error: 'Please confirmed your account to login.'}
    else
      redirect_to new_user_session_path, flash: {error: 'Invalid credential.'}
    end
  end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end
  # private
  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
end
