## Organization Controller
class OrganizationsController < ApplicationController
  ## Find Uniqueness validation of Organization
  def getOrganization
  	organization = Organization.find_by(name: params[:user][:organization_name].downcase)  
  	render json: organization.present? ? false : true  	
  end
end
