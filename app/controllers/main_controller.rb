class MainController < ApplicationController
  before_action :authenticate_user!
  around_action :set_current_user
  
  layout :get_layout

  include ApplicationHelper
  include Current

	def set_current_user
    Current.user = current_user
    Current.organization = current_organization
    yield
  ensure
    Current.user = nil
    Current.organization = nil
  end  

  def get_layout
    if current_role == 'organization_owner'
      'dashboard'
    else
      'users'
    end    
  end
end
