class LettersController < MainController
	
	expose :letters, ->{ @current_organization.letters }
	expose :letter, find_by: :slug

  add_breadcrumb 'Home', :user_dashboard_path
  add_breadcrumb 'Letters', :letters_path

  include GenerateLetterPdf

  def new
  	add_breadcrumb 'New'
  end

  def create
  	letter = @current_organization.letters.build(letter_params)
  	if letter.save
  		redirect_to letters_path, notice: 'Successfully added letter!'
  	else
  		render action: :new
  	end
  end

  def edit
  	add_breadcrumb 'Edit'
  end

  def update
  	if letter.update(letter_params)
  		redirect_to letter, notice: 'Successfully updated letter !'
  	else
  		render action: :edit
  	end
  end

  def destroy
    letter.destroy if letter.present?
    redirect_to letters_path, notice: 'Successfully deleted letter !'
  end

  def show
  	add_breadcrumb 'View'
  end

  def generate_pdf
    respond_to do |format|
      format.html{redirect_to letters_path}
      format.js
    end    
  end

  def download_pdf
    if params[:letter][:organization_title].present?
      @current_organization.update_column(:title, params[:letter][:organization_title])
    end
    if params[:profile].present?
      employee = @current_organization.organizations_users.find_by(user_id: params[:letter][:employee_id])
      employee.profile.update_column(:designation, params[:profile][:designation])      
    end
    generate_pdf_for_letters(letter, params[:letter], @current_organization)    
  end

  def user_info
    @employee = @current_organization.organizations_users.find_by(user_id: params[:employee_id])
    respond_to do |format|
      format.html{render nothing: true}
      format.js
    end
  end

  private

  def letter_params
  	params.require(:letter).permit(:title, :description, :organization_id)
  end

end
