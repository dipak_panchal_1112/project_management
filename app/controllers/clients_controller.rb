class ClientsController < BaseController
	expose(:client)
  expose(:project, find_by: :slug)

  def new
  	respond_to do |format|
  		format.html{ redirect_to edit_project_path(client.project)}
  		format.js
  	end
  end

  def create
  	client = project.clients.build(client_params)
  	if client.save
  		message = {notice: 'Successfully created client for your project !'}
  	else
  		message = {error: client.errors.full_messages.join(', ')}
  	end
  	respond_to do |format|
  		format.html{ redirect_to edit_project_path(client.project), flash: message }
  		format.js
  	end
  end

  def edit
  	respond_to do |format|
  		format.html{ redirect_to edit_project_path(client.project)}
  		format.js
  	end  	
  end

  def update
		if client.update(client_params)
			message = { notice: 'Successfully updated client' }
		else
			message = { error: client.errors.full_messages.join(', ')}
		end  	
  	respond_to do |format|
  		format.html{ redirect_to edit_project_path(client.project), flash: message }
  		format.js
  	end
  end

  def destroy
  	client.destroy if client.present?
  	respond_to do |format|
  		format.html { respond_to edit_project_path(client.project), flash: {notice: 'Successfully deleted client.'} }
  		format.json{
  			render json: {id: client.uniq_number, total: project.clients.count, url: "/projects/#{client.project.slug}/edit", isClient: true}, status: 200
  		}
  	end
  end

  private

  def client_params
  	params.require(:client).permit(:fullname, :company_name, :email, :email2, :phone_number, :mobile_number, :address, :skype, :user_id)
  end
end
