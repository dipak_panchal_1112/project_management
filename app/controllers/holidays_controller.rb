require 'csv'
class HolidaysController < MainController

	skip_before_action :verify_authenticity_token, only: [:import_holiday]

	expose :holidays, ->{ @current_organization.holidays }
	expose :holiday

  add_breadcrumb 'Home', :user_dashboard_path
  add_breadcrumb 'Holiday', :holidays_path
	
	def create
		holiday = @current_organization.holidays.build(holiday_params)
		holiday.save if holiday.valid?
		respond_to do |format|
			format.html{redirect_to hr_holidays_path, notice: 'Successfully added holiday for your organization!'}
			format.js
		end
	end

	def edit
		respond_to do |format|
			format.html{ redirect_to hr_holidays_path }
			format.js
		end
	end

	def update
		holiday.update(holiday_params)
		respond_to do |format|
			format.html{ redirect_to hr_holidays_path, notice: 'Successfully updated holiday!' }
			format.js
		end		
	end

	def destroy
		holiday.destroy if holiday.present?
  	respond_to do |format|
  		format.html{
  			redirect_to hr_holidays_path, flash: {notice: 'Successfully deleted holiday !'}
  		}
  		format.json{
  			render json: {id: holiday.uniq_number, total: @current_organization.holidays.count, url: '/holidays'}, status: 200
  		}
  	end		
	end

	def import
		respond_to do |format|
			format.html{ redirect_to hr_holidays_path }
			format.js
		end
	end

	def import_holiday
		case params[:holiday][:import_by].to_s
		when '1' ## All Saturday Off
			all_saturday_off
		when '2', '3' ## 1st, 3rd and 5th Saturday off			
			custom_saturday_off(params[:holiday][:import_by].to_s)
		when '4' ## Custom File Upload
			@error = custom_holiday_upload(params[:holiday][:file])
		end
		respond_to do |format|
			format.html{ redirect_to hr_holidays_path }
			format.js
		end		
	end

	def calendar
		holiday_list = []
		@current_organization.holidays.select('id, name, hdate').each do |holiday|
			holiday_list << {title: holiday.name, start: holiday.hdate.to_date.to_s, allDay: true}
		end
		gon.holiday_list = holiday_list
	end

	def getExistHoliday
		if params[:id].present?
    	holidays = @current_organization.holidays.where('DATE(hdate) = ? and id != ?', params[:holiday][:hdate].to_date, params[:id])
    else
    	holidays = @current_organization.holidays.where('DATE(hdate) = ?', params[:holiday][:hdate].to_date)
    end
    render json: holidays.present? ? false : true		
	end

	private

	def holiday_params
		params.require(:holiday).permit(:name, :hdate, :htype, :import_by, :file)
	end

	def all_saturday_off
		start_date 	= "1/1/#{Date.today.year}".to_date
		end_date 		= "31/12/#{Date.today.year}".to_date
		result 	= (start_date..end_date).to_a.select {|k| [6].include?(k.wday)}
		result.each do |sat|
			find_or_create_holiday(sat)
		end
	end

	def custom_holiday_upload(file)		
		error = nil
		if file.content_type == 'text/csv'
			CSV.foreach(file.path, :headers => true) do |row|
				begin
					find_or_create_holiday(row['holiday_date'])
				rescue => e
					error = e
				end
			end
		else
			error = 'Invalid file format'
		end		
	end

	def custom_saturday_off(imtype)
		start_date = "1/1/#{Date.today.year}".to_date
		end_date = "31/12/#{Date.today.year}".to_date
		result 	= (start_date..end_date).to_a.select {|k| [6].include?(k.wday)}
		result.group_by{|mon| mon.month}.each do |month, sats|
			sats.each_with_index do |sat, index|
				index += 1
				case imtype.to_s
				when '2'
					find_or_create_holiday(sat) if index%2 != 0
				when '3'					
					find_or_create_holiday(sat) if index%2 == 0
				end
			end
		end		
	end

	def find_or_create_holiday(sat)
		hld = @current_organization.holidays.where('DATE(hdate) = ?', sat.to_date)
		if !hld.present?
			@current_organization.holidays.create(hdate: sat.to_date, name: 'Saturday Off', htype: 2)
		end
	end
end
