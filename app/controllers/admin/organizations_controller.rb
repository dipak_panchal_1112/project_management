class Admin::OrganizationsController < AdminController
	expose :organizations, ->{ Organization.search(params[:search]) }
  expose(:organization, finder_parameter: :slug)

  before_action :organization, only: [:show, :edit]

  add_breadcrumb 'Home', :admin_dashboard_path
  add_breadcrumb 'Organizations', :admin_organizations_path

  def show
		add_breadcrumb organization.name
  end

  def edit
  	add_breadcrumb organization.name, [:admin, organization]
  	add_breadcrumb 'Edit'
  end

  def update
  	if organization.update(organization_params)
  		redirect_to admin_organizations_path, notice: 'Successfully updated organization profile.'
  	else
  		render action: :edit
  	end
  end
  
  def destroy
    organization.destroy if organization.present?
    respond_to do |format|
      format.html{
        redirect_to admin_organizations_path, flash: {error: 'Successfully deleted organization !'}
      }
      format.json{
        render json: {id: params[:id], url: '/admin/organizations'}, status: 200
      }     
    end    
  end

  private

  def organization_params
  	params.require(:organization).permit(:about_us, :title, :name, :email, :address, :contact_number, :website, :status_id, :logo)
  end

  def organization
  	Organization.find_by(slug: params[:id])
  end

end
