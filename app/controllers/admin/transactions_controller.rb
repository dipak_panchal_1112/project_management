class Admin::TransactionsController < AdminController
	expose :transactions, ->{ Transaction.latest }
	expose :transaction
  
  before_action :transaction, only: [:show]

  add_breadcrumb 'Home', :admin_dashboard_path
  add_breadcrumb 'Transactions', :admin_transactions_path

  def show
		add_breadcrumb 'View'
  end

  private

  def transaction
  	Transaction.find_by(id: params[:id])
  end
end
