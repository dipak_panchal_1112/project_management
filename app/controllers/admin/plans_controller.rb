class Admin::PlansController < AdminController
	expose :plans, ->{ Plan.all }
  expose(:plan)

  before_action :plan, only: [:edit, :show]

  add_breadcrumb 'Home', :admin_dashboard_path
  add_breadcrumb 'Plans', :admin_plans_path

  def create
  	if plan.save
  		redirect_to admin_plans_path, notice: 'Successfully created plan !'
  	else
  		render action: :new
  	end
  end

  def edit
  	add_breadcrumb plan.name, [:admin, plan]
  	add_breadcrumb 'Edit'
  end

  def update
  	if plan.update(plan_params)
  		redirect_to admin_plans_path, notice: 'Successfully updateds plan !'
  	else
  		render action: :edit
  	end
  end

  def show
  	add_breadcrumb plan.name
  end

  ## This action is used for validation
  def getPlan
  	plan = Plan.find_by(name: params[:plan][:name])
  	render json: plan.present? ? false : true
  end

  def destroy
  	plan.destroy if plan.present?
  	respond_to do |format|
  		format.html{
  			redirect_to admin_plans_path, flash: {error: 'Successfully deleted plan !'}
  		}
  		format.json{
  			render json: {id: params[:id], total: Plan.count, url: '/admin/plans'}, status: 200
  		}
  	end
  end

  private

  def plan_params
  	params.require(:plan).permit(:name, :title, :price, :users, :storage, :primary)
  end

  def plan
  	Plan.find_by(id: params[:id])
  end

end
