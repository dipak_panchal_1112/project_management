class Secure::TaskAttachmentsController < MainController
	expose(:task_attachment)
	expose :project, fetch: ->{ get_project }
	expose :task, fetch: ->{ get_task }

	def destroy
		task_attachment.destroy if task_attachment.present?
		respond_to do |format|
			format.html{render nothing: true}
			format.js
		end
	end

	def download
		data = open(task_attachment.document.url)
		send_data data.read, filename: task_attachment.document_file_name, type: task_attachment.document_content_type, disposition: 'attachment'
	end

	private

	def get_project
 		Project.find_by(slug: params[:project_id])
 	end

 	def get_task
 		Task.find_by(slug: params[:task_id])
 	end	
end
