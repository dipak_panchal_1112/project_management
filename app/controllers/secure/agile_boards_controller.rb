class Secure::AgileBoardsController < MainController

  add_breadcrumb 'Home', :user_dashboard_path
  add_breadcrumb 'Projects', :secure_projects_path
  add_breadcrumb 'Agile Board'

  expose(:project, find_by: :slug)
  expose :tasks, fetch: ->{ get_project_tasks }

  def add_task
  	@task = project.tasks.create(task_params)
  end
  
  private

  def get_project_tasks  	
  	project.tasks
  end

  def task_params
  	params.require(:task).permit(:summary, :project_id)
  end

end
