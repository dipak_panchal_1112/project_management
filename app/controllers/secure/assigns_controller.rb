class Secure::AssignsController < MainController

	expose :project, fetch: ->{ get_project }
	expose :task, fetch: ->{ get_task }

	def new
		respond_to do |format|
			format.html{render nothing: true}
			format.js
		end
	end
	
	def create
    respond_to do |format|
      if task.update(task_params)
        format.html{ redirect_to secure_project_task_path(task.project, task), notice: 'Successfully updated task !' }
        format.json{ render json: {status: 200} }
      else      	
        format.html{ render action: :edit }
        format.json { render json: task.errors, status: :unprocessable_entity }
      end
      format.js
    end		
	end

	private

 	def task_params
 		params.require(:task).permit(:summary, :status_id, :estimate_time, :issue_type_id, :reporter_id, :description, :priority, user_ids: [], comments_attributes: [:id, :content, :user_id, :project_id]).merge({creator_id: current_user.id})
 	end

	def get_project
 		Project.find_by(slug: params[:project_id])
 	end

 	def get_task
 		Task.find_by(slug: params[:task_id])
 	end	

end
