class Secure::CommentsController < MainController

	expose :comment, build: ->(comment_params, scope){ scope.new(comment_params) }
	expose :task, fetch: ->{ get_task }	
	expose :project, find: ->(id, scope){ scope.find_by!(slug: id) }
	expose :my_comment, fetch: ->{ get_my_comment }

	def new
		respond_to do |format|
			format.html{render nothing: true}
			format.js
		end
	end

	def create
		@comment = task.comments.build(comment_params) if task.present?
		@comment = project.comments.build(comment_params) if !task.present?
		@comment.save if @comment.valid?
		respond_to do |format|
			format.html{render nothing: true}
			format.js
		end
	end

	def edit
		respond_to do |format|
			format.html{render nothing: true}
			format.js
		end
	end

	def update		
		my_comment.update(comment_params)
		respond_to do |format|
			format.html{ render nothing: true}
			format.js
		end
	end

	def destroy
		my_comment.destroy
		respond_to do |format|
			format.html{ render nothing: true}
			format.json {render json: {id: "task-comment-#{params[:id]}" }, status: 200}
		end
	end

	private

	def comment_params
		params.require(:comment).permit(:user_id, :content, attachment_ids: []).merge(user_id: current_user.id, task_id: task.present? ? task.id : nil, project_id: project.id)
	end

	def get_task
		if params[:task_id] && params[:task_id].present?
			Task.where('id = ? OR slug = ?', params[:task_id].to_i, params[:task_id]).try(:last)
		end		
	end

	def get_my_comment
		if params[:id] && params[:id].present?
			Comment.find_by(id: params[:id])
		end
	end
end
