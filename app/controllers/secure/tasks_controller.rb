class Secure::TasksController < MainController	
  
  expose(:task, find_by: :slug)
  expose :project, fetch: ->{ get_project }
  expose :tasks, fetch: ->{ get_project_tasks }

  skip_before_action :verify_authenticity_token, only: [:store_attachments]

  include UsersHelper
  include TaskWorkLog

  add_breadcrumb 'Home', :user_dashboard_path
  add_breadcrumb 'Projects', :projects_path

  before_action :set_attributes_to_gon, except: [:new, :create, :index, :update, :store_attachments]  

  def new
    @token = Task.generate_token    
  	respond_to do |format|
  		format.html{redirect_to secure_projects_path}
  		format.js
  	end
  end

  def subtask
    respond_to do |format|
      format.html{redirect_to secure_projects_path}
      format.js
    end    
  end

  def show
    add_breadcrumb project.name, project_path(project)
    add_breadcrumb 'Tasks'
  end

  def create
  	task = project.tasks.build(task_params)
   	if task.save
      attachments = project.task_attachments.where('token = ?', params[:token])
      if attachments.present?
        attachments.update_all(task_id: task.id)
        attachments.update_all(token: nil)
      end
  		redirect_to secure_project_path(project), notice: 'Successfully created task !'
  	else
  		render action: :new
  	end
  end

  def update
    respond_to do |format|
      if task.update(task_params)
        format.html{ redirect_to secure_project_task_path(task.project, task), notice: 'Successfully updated task !' }
        format.json{ render json: {status: 200} }
        format.js
      else
        format.html{ render action: :edit }
        format.json { render json: task.errors, status: :unprocessable_entity }
      end
    end
  end

  def store_attachments
    attachment = project.task_attachments.build({
      document: params['file'], 
      user: current_user, 
      task_id: params[:id].present? ? task.id : nil,
      token: params[:token].present? ? params[:token] : nil
    })
    if attachment.save
      render json: {status: 200}
    else
      render json: attachment.errors, status: :unprocessable_entity
    end
  end

  def destroy
    task.destroy if task.present?
    redirect_to secure_project_path(project), notice: 'Successfully deleted task !'
  end

  private

 	def get_project
 		Project.find_by(slug: params[:project_id])
 	end

 	def task_params
 		params.require(:task).permit(:summary, :status_id, :estimate_time, :issue_type_id, :reporter_id, :description, :ancestry, :priority, user_ids: [], comments_attributes: [:id, :content, :user_id, :project_id]).merge(creator_id: current_user.id)
 	end

  def get_project_tasks   
    project.tasks.search(params)
  end

  def set_attributes_to_gon
    gon.push({
      task_slug: task.slug,
      project_slug: project.slug,
      reporter_list: project.users.collect{|t| {value: t.id, text: "#{t.fullname} [ #{role_for_current_organization(t, @current_organization)} ]" } }
    })
  end
end
