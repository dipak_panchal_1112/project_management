class Secure::WatchersController < MainController

	expose :project, fetch: ->{ get_project }
	expose :task, fetch: ->{ get_task }	

	def new
		respond_to do |format|
			format.html{redirect_to secure_project_task_path(project, task) }
			format.js
		end
	end

	def create
		Watcher.update_watchlist(params, current_user, task)		
		respond_to do |format|
			format.html{redirect_to secure_project_task_path(project, task) }
			format.js
		end
	end

	private

	def get_project
 		Project.find_by(slug: params[:project_id])
 	end

 	def get_task
 		Task.find_by(slug: params[:task_id])
 	end	

end
