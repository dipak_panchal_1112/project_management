class Secure::ProjectsController < MainController

  expose :projects, ->{ current_user.projects.search(params[:search]) }
  expose(:project, find_by: :slug)
  expose :tasks, fetch: ->{ get_project_tasks }

  add_breadcrumb 'Home', :user_dashboard_path
  add_breadcrumb 'Projects', :secure_projects_path

 	def show
 		if params[:task].present?
 			task = project.tasks.where("status_id = ?", params[:status]).order('created_at ASC').try(:first)
 			if task.present?
 				redirect_to secure_project_task_path(project, task, status: params[:status])
			else
				redirect_to secure_project_path(project, status: params[:status])
			end 				
 		end
 	end

  def reports
    add_breadcrumb project.name, secure_project_path(project)
    add_breadcrumb 'Chart'
    @pieChartReportForProject = []
    @report2 = []
    if project.tasks.present?
      if params[:start_date].present? && params[:end_date].present?
        proj_tasks = project.tasks.where('DATE(created_at) >= ? AND DATE(created_at) <= ?', params[:start_date].to_date, params[:end_date].to_date).select('id, status_id, issue_type_id')
      else
        proj_tasks = project.tasks.select('id, status_id, issue_type_id')
      end
      report1 = proj_tasks.group_by{|t| t.status}
      report2 = proj_tasks.group_by{|t| t.issue_type}
      if report1.present?
        report1.each do |status, ptasks|
          @pieChartReportForProject << {label: status, data: ptasks.flatten.count, color:  ptasks.first.status_color}
        end
      end

      if report2.present?
        report2.each do |itype, ptasks|
          @report2 << {label: itype, data: ptasks.flatten.count, color:  ptasks.first.issue_type_color}
        end
      end
    end    
    gon.pieChartReportForProject = @pieChartReportForProject
    gon.report2 = @report2
  end

  private

  def get_project_tasks  	
  	project.tasks.only_parents.search(params)
  end

end
