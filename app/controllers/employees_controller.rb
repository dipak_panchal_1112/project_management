class EmployeesController < MainController

	expose :employees, ->{ current_organization.organizations_users }
  expose :employee, fetch: ->{ get_employee }
  expose :current_organization_user, fetch: -> {get_current_org_employee}
  expose :current_profile, fetch: -> {get_current_org_employee_profile}
  

  add_breadcrumb 'Home', :user_dashboard_path
  add_breadcrumb 'Employees', :employees_path

  def edit
  	add_breadcrumb 'Edit'
  end

  def update
    respond_to do |format|
      if current_profile.update(employee_params)
        format.html{redirect_to employees_path, notice: 'Successfully updated employee detail.'}
      else
        format.html{render action: :edit}
      end
      format.js
    end
  end
  
  def employee_document_update
    respond_to do |format|
      if current_organization_user.update(employee_document_params)
        format.html{redirect_to employees_path, notice: 'Successfully updated employee detail.'}
      else
        format.html{render action: :edit}
      end
      format.js
    end
  end

  private

  def get_employee
  	current_organization.users.find_by(slug: params[:id])
  end
  
  def employee_params
    params.require(:profile).permit(:ifsc_code, :education_status, :status, :bank_name, :account_number, :avatar, :firstname, :lastname, :phone_number, :home_number, :mobile_number, :website, :address, :city, :country, :state, :zipcode, :about_me, :birth_date, :designation, :marriage_date, :blood_group, :appointment_type, :personal_email, :skype, :personal_skype, :same_as_current_address, :permanent_address, :permanent_city, :permanent_country, :permanent_state, :permanent_zipcode, :parent_name, :parent_phone_number, :relationship_with_parent, :parent_address, :parent_city, :parent_country, :parent_state, :parent_zipcode, :gender, :joining_date, :resign_date, profile2s_attributes: [:id, :company_name, :start_date, :end_date, :reason_for_resignation, :'_destroy'], certificates_attributes: [:id, :name, :klass, :month, :year, :'_destroy'])
  end

  def employee_document_params
    params.require(:organizations_user).permit(:status_id, employee_documents_attributes: [:document_name, :document, :'_destroy', :id, :user_id, :orgination_id])
  end

  def get_current_org_employee
    if employee.present?    
      current_organization.organizations_users.find_by(user_id: employee.id)
    end
  end

  def get_current_org_employee_profile
    if current_organization_user.present?
      current_organization_user.profile
    end
  end
end
