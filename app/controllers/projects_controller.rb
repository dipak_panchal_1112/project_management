class ProjectsController < BaseController

  expose :projects, ->{ current_organization.projects.search(params[:search]) }
  expose(:project, find_by: :slug)

  add_breadcrumb 'Home', :user_dashboard_path
  add_breadcrumb 'Projects', :projects_path

  def new
  	respond_to do |format|
  		format.html{redirect_to projects_path}
  		format.js
  	end
  end

  def create
    project = current_organization.projects.build(project_params)
    project.save if project.valid?
    respond_to do |format|
      format.html{redirect_to projects_path, notice: 'Successfully created projects'}
      format.js
    end    
  end

  def edit
    add_breadcrumb 'Edit'
    gon.project_id = project.id
  end

  def update
    if project.update(update_project_params)
      redirect_to projects_path, notice: 'Successfully updated project !'
    else
      render action: :edit
    end
  end

  def invitation
    if project.user_ids.include?(params[:project][:user_ids].to_i)
      msg = 'User already invited'
    else      
      project.user_ids =  project.user_ids << params[:project][:user_ids].to_i
      msg = 'Successfully send invitation to user !'
    end
    respond_to do |format|
      format.html{redirect_to project, notice: msg}
      format.js
    end
  end

  def getProNameUniq
    if params[:project_id].present?
      project = Project.where('lower(name) = ? AND organization_id = ? AND id != ?', params[:project][:name].downcase, current_organization.id, params[:project_id]).try(:last)
    else
      project = Project.where('lower(name) = ? AND organization_id = ?', params[:project][:name].downcase, current_organization.id).try(:last)
    end
    render json: project.present? ? false : true
  end

  private

  def project_params
    params.require(:project).permit(:name, :description)
  end

  def update_project_params
    params.require(:project).permit(:name, :description, :start_date, :end_date, :status_id, :url, :user_ids, keywords: [], 
      clients_attributes: [:id, :fullname, :company_name, :email, :email2, :phone_number, :mobile_number, :skype, :address, :user_id, '_destroy', :project_id]
    )
  end

end
