# Main Welcome Controller for Home Page
class WelcomeController < ApplicationController

  expose :plans, ->{ Plan.all }

	layout 'home'

	before_action :authenticate_user!, except: [:index, :thank_you]
  before_action :go_to_dashboard

  def index
  end

  def thank_you
  	if session[:domain].present?
  		@domain 					= session[:domain]
  		session[:domain] 	= nil
      render layout: 'login'
  	else
  		redirect_to root_path
  	end   	
  end

  private

  def go_to_dashboard
    if current_user.present? && current_role == 'super_admin'
      redirect_to admin_dashboard_path
    elsif current_user.present? && current_role != 'super_admin'
      redirect_to user_dashboard_path
    end
  end
end
