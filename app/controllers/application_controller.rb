class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :current_organization, :current_role
  before_action :lock_screen_redirection, except: [:lock_screen]
  
  include Current
  include ApplicationHelper
  

  ## For current organization
  def current_organization  	
    @current_organization ||= Organization.find_by(name: request.subdomain)
    @current_organization
  end

  def current_role
  	if current_user.present?
  		@current_role = current_user.organizations_users.find_by(organization_id: @current_organization).my_role
  	else
  		@current_role = 'Unknown'
  	end
  end

  def lock_screen_redirection    
    if current_user.present? && !is_org_owner?
      orgnization_user = current_user.organizations_users.find_by(organization_id: @current_organization)
      if !orgnization_user.active?
        redirect_to lock_screen_users_path 
      end
    end
  end
end
