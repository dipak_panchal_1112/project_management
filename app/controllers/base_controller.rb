class BaseController < ApplicationController
  before_action :authenticate_user!
  layout 'dashboard'
  
  include ApplicationHelper
  include Current
end
