require 'json'
class TransactionsController < ApplicationController
	skip_before_filter  :verify_authenticity_token

	def authorize_silent_post
		subscription = Subscription.find_by(subscription_id: params[:x_subscription_id])
		if subscription && subscription.present?
			subscription.transactions.build({
				organization_id: subscription.organization,
				amount: params[:x_amount].to_f.round(2),
				status: params[:x_response_code].to_s == '1' ? 'Success' : 'Fail',
				response_code: params[:x_response_reason_code],
				response_reason_text: params[:x_response_reason_text],
				transaction_id: params[:x_trans_id],
				response: JSON.parse(params.to_json)
			})
		else ## For Checking response when subscription is not present
			Transaction.create(response: JSON.parse(params.to_json))
		end
		render json: {response: :success}, status: 200
	end
end