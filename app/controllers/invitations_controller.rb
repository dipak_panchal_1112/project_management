class InvitationsController < BaseController
	## Decent Expose 
	skip_before_action :authenticate_user!, only: [:accept, :thank_you, :accept_invitation]

	expose :invitations, ->{ current_organization.invitations.with_tokens }
  expose(:invitation)

  ## BreadCrumb
  add_breadcrumb 'Home', :user_dashboard_path
  add_breadcrumb 'Users', :invitations_path

  def create
  	@invitation = current_user.invitations.build(invitation_params)
  	@invitation.save! if @invitation.valid?
  	respond_to do |format|
  		format.html{ 
  			redirect_to users_path, notice: "Successfully send invitation to #{@invitation.invite_email} !"
  		}
  		format.js
  	end
  end

  def destroy
  	invitation.destroy if invitation.present?
  	respond_to do |format|
  		format.html{
  			redirect_to invitations_path, flash: {error: 'Successfully deleted invitation !'}
  		}
  		format.json{
  			render json: {id: invitation.uniq_number, total: current_organization.invitations.count, url: '/users', remove: true}, status: 200
  		}
  	end  	
  end

  def accept
  	return redirect_to root_path if !params[:token].present?
  	@invitation = Invitation.find_by(token: params[:token])
  	return redirect_to root_path if !@invitation.present?
  	return redirect_to root_path if current_organization.id != @invitation.invited_by_organization_id
  	user = User.find_by(email: @invitation.invite_email)
  	if user.present?
  		session[:existMember] = 'Yes'
      org_user = user.organizations_users.create(organization_id: @invitation.invited_by_organization_id, role_id: @invitation.role_id, status_id: 1)
      org_user.build_profile(firstname: org_user.user.firstname, lastname: org_user.user.lastname, organization_id: org_user.organization_id, user_id: user.id)
      org_user.save
  		@invitation.update_attributes(token: nil, invitation_accepted_at: Time.zone.now)
      redirect_to thank_you_invitations_path
  	else  		
  		@user = User.new
  		render layout: 'invitation'
  	end  	
  end

  def thank_you
  	if session[:existMember].present?
  		@existMember 						= session[:existMember]
  		session[:existMember] 	= nil
  		render layout: 'invitation'
  	else
  		redirect_to root_path
  	end
  end

  def accept_invitation
  	return redirect_to root_path, flash: {error: 'Invalid token to accept the invitation!'} if !params[:token].present?
  	invitation = Invitation.find_by(token: params[:token])
  	return redirect_to root_path, flash: {error: 'Invalid token to accept the invitation!'} if !invitation.present?
  	user 				= User.new(user_params)
  	user.email 	= invitation.invite_email
  	user.organization_id = current_organization.id  	
  	if user.save
  		org_user = user.organizations_users.create(organization_id: invitation.invited_by_organization_id, role_id: invitation.role_id, status_id: 1)
      ## Profile
      org_user.build_profile(firstname: org_user.user.firstname, lastname: org_user.user.lastname, organization_id: org_user.organization_id, user_id: user.id)
      org_user.save

  		## Update Invitation for not use again the token
  		invitation.update_attributes(token: nil, invitation_accepted_at: Time.zone.now)
  		user.update_attributes(organization_id: nil)
  		session[:existMember] = 'No'
  		redirect_to thank_you_invitations_path, notice: 'Welcome to project management system ! '
  	else
  		redirect_to root_path, flash: {error: 'Something is wrong to accept the invitation. Please do it again !'}
  	end
  end

  def existMember
  	## Here we check that the invite_email is already member of the current organization or not.
  	email = params[:invitation][:invite_email]
  	member = OrganizationsUser.includes([:user, :organization]).where('users.email = ? and organizations.id = ?', email, @current_organization).references([:user, :organization]).try(:last)  	
  	render json: member.present? ? false : true
  end

  private

  def invitation_params
  	params.require(:invitation).permit(:invite_email, :role_id, :invited_by_user_id, :invited_by_organization_id).merge(invited_by_organization_id: @current_organization.id)
  end

  def user_params
  	params.require(:user).permit(:firstname, :lastname, :email, :username, :password, :password_confirmation)
  end

end
