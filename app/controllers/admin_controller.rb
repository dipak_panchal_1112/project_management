## This is Admin Controller. Admin related functionality. 
class AdminController < ApplicationController

	before_action :authenticate_user!, :only_admin_user
	
	include ApplicationHelper

	layout 'dashboard'
	
	def dashboard
    add_breadcrumb "Dashboard"
	end
	
	private

	def only_admin_user
		if not is_super_admin?
			redirect_to root_path
		end
	end

end
