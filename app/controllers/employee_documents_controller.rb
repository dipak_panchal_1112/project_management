class EmployeeDocumentsController < MainController
	expose(:employee_document)
	expose :employee, fetch: ->{ get_employee }	

	def download
		data = open(employee_document.document.url)
		send_data data.read, filename: employee_document.document_file_name, type: employee_document.document_content_type, disposition: 'attachment'		
	end

	def destroy
		employee_document.destroy if employee_document.present?
  	respond_to do |format|
  		format.html{
  			redirect_to "/employees/#{params[:employee_id]}/edit", flash: {notice: 'Successfully deleted document !'}
  		}
  		format.json{
  			render json: {id: employee_document.uniq_number, total: employee.employee_documents.count, url: "/employees/#{params[:employee_id]}"}, status: 200
  		}
  	end		
	end

	private

	def get_employee
		current_organization.users.find_by(slug: params[:employee_id])
	end

end
