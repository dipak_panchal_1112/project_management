# == Schema Information
#
# Table name: certificates
#
#  id         :integer          not null, primary key
#  profile_id :integer
#  name       :string
#  year       :integer          default(0)
#  month      :integer          default(0)
#  klass      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Certificate < ApplicationRecord
	belongs_to :profile
end
