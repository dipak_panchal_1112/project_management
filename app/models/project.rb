# == Schema Information
#
# Table name: projects
#
#  id              :integer          not null, primary key
#  name            :string
#  description     :text
#  start_date      :date
#  end_date        :date
#  total_hour      :float            default(0.0)
#  url             :text
#  status_id       :integer          default(1)
#  organization_id :integer
#  slug            :string
#  key             :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  keywords        :text
#

class Project < ApplicationRecord
	## Friendly ID
	extend FriendlyId
  friendly_id :name, use: :slugged
	
	## Callbacks
	before_create :generate_short_uniq_key

	## Relationship
	belongs_to :organization

  has_many :projects_users
  has_many :users, through: :projects_users
  
  has_many :clients, -> {order 'created_at ASC'}
  accepts_nested_attributes_for :clients, allow_destroy: true

  has_many :task_attachments
  accepts_nested_attributes_for :task_attachments, allow_destroy: true

  has_many :tasks, -> {order 'created_at DESC'}
  accepts_nested_attributes_for :tasks, allow_destroy: true
  
  has_many :comments, -> {order 'created_at DESC'}

  ## Validations
  validates :name, :description, presence: true
  validates :name, uniqueness: {scope: :organization}

  serialize :keywords, Array

	## Callback methods
	def generate_short_uniq_key
		result 		= name.split.map(&:first).join
		self.key 	= result.length < 3 ? name[0..2].upcase : result.upcase
	end

	## Method
	def status
		Rails.application.secrets.project_statuses[status_id]
	end

  def self.search(srch=nil)
    if srch.present?
      #where('email LIKE ? or firstname LIKE ? or lastname LIKE ?', "%#{srch}%", "%#{srch}%", "%#{srch}%").order('created_at DESC')
    else
      order('created_at DESC')
    end
  end

  def last_updated
  	updated_at.strftime('%d/%m/%Y')
  end

  def created_date
  	created_at.strftime('%d/%m/%Y')
  end

  def to_start_date
    start_date? ? start_date.strftime('%d %b, %Y') : ''
  end

  def to_end_date
    end_date? ? end_date.strftime('%d %b, %Y') : 'Not Define'
  end
end
