# == Schema Information
#
# Table name: transactions
#
#  id                   :integer          not null, primary key
#  organization_id      :integer
#  subscription_id      :integer
#  subscription_number  :string
#  amount               :float            default(0.0)
#  status               :string
#  response_code        :string
#  response_reason_text :string
#  transaction_id       :string
#  response             :text
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class Transaction < ApplicationRecord

	## Relationships
	belongs_to :organization
	belongs_to :subscription
	## Scopes
	scope :latest, -> { order('created_at DESC')}
	
	serialize :response, Hash

	def status_name
		if status.present?
			Rails.application.secrets.transaction_statuses[status.to_i]
		else
			'Unknown'
		end
	end

	def my_subscription_id
		if subscription_number.present?
			subscription_number
		else
			subscription.try(:subscription_id)
		end
	end
end
