# == Schema Information
#
# Table name: employee_documents
#
#  id                    :integer          not null, primary key
#  user_id               :integer
#  document_file_name    :string
#  document_content_type :string
#  document_file_size    :integer
#  document_updated_at   :datetime
#  document_name         :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  organizations_user_id :integer
#

class EmployeeDocument < ApplicationRecord
	## Relationship
	belongs_to :user
	belongs_to :organizations_user

	## paperclip
  has_attached_file :document,
    storage: :s3,
    s3_region: 'us-west-2',
    s3_credentials: "#{Rails.root}/config/amazon_s3.yml",
    path: "/employee-documents/:id/:style/:filename",
    url: "/employee-documents/:id/:style/:filename"

  ## Validations
  validates :document_name, presence: true

  ## Callbacks
  before_create :set_user_id_and_organization_id

  def set_user_id_and_organization_id
    self.user_id = Current.user.id    
  end

  def uniq_number
    unumber = "#{id}".rjust(2, '0')
    "EMPDOC0#{unumber}#{created_at.to_i}".downcase
  end
end
