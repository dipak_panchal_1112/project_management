# == Schema Information
#
# Table name: tasks_users
#
#  id         :integer          not null, primary key
#  task_id    :integer
#  user_id    :integer
#  total_min  :integer          default(0)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class TasksUser < ApplicationRecord
	
	## Callbacks
	before_create :add_histories_for_assignee
	before_destroy :add_histories_for_delete_assignee

	## Relationships
	belongs_to :task
	belongs_to :user

	def add_histories_for_assignee
    task.histories.create({
      user_id: Current.user.id, 
      message: "added assignee <b>#{user.fullname}</b>",
      change_type: 'TaskUpdate'
    })
	end

	def add_histories_for_delete_assignee		
    task.histories.create({
      user_id: Current.user.id, 
      message: "remove assignee <b>#{user.fullname}</b>",
      change_type: 'TaskUpdate'
    })		
	end

end
