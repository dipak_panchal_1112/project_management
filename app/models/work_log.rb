# == Schema Information
#
# Table name: work_logs
#
#  id              :integer          not null, primary key
#  task_id         :integer
#  user_id         :integer
#  organization_id :integer
#  log_time        :integer          default(0)
#  status_id       :integer
#  message         :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class WorkLog < ApplicationRecord
	## Relationship
	belongs_to :task
	belongs_to :user
	belongs_to :organization
end
