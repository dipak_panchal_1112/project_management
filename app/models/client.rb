# == Schema Information
#
# Table name: clients
#
#  id              :integer          not null, primary key
#  project_id      :integer
#  fullname        :string
#  address         :string
#  email           :string
#  phone_number    :string
#  mobile_number   :string
#  email2          :string
#  skype           :string
#  user_id         :integer
#  organization_id :integer
#  company_name    :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Client < ApplicationRecord
	## Relaitonship
	belongs_to :project
	belongs_to :user
	belongs_to :organization
	
	def client_phone_number
		cpn = [phone_number, mobile_number].reject{|n| !n.present? }.join(', ')
		user_id? ? user.profile.to_phone_number : cpn
	end

	def client_emails
		user_id? ? user.email : [email, email2].reject{|n| !n.present?}.join(', ')
	end

	def uniq_number
		unumber = "#{id}".rjust(4, '0')
		"CL0#{created_at.to_i}#{unumber}"
	end

	def user_fullname
		user_id.present? ? user.fullname : fullname
	end
end
