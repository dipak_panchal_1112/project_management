# == Schema Information
#
# Table name: comments
#
#  id             :integer          not null, primary key
#  task_id        :integer
#  user_id        :integer
#  attachment_ids :string
#  content        :text
#  project_id     :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Comment < ApplicationRecord
	## Relationship
	belongs_to :task
	belongs_to :user
	belongs_to :project
	
	serialize :attachment_ids, Array

	include ActionView::Helpers::SanitizeHelper

	## Vallidation
	validates :content, presence: true
	validate :valid_content

	## Methods
	def attachments
		if task_id? && task_id.present?
			task.task_attachments.where("id IN (?)", attachment_ids.reject{|t| !t.present? })
		else
			[]
		end
	end

	## Custom Validation

	def valid_content
		if strip_tags(content) == ''
			errors.add(:content, "can't be blank !")
		end
	end

end
