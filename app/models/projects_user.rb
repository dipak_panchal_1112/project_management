# == Schema Information
#
# Table name: projects_users
#
#  id              :integer          not null, primary key
#  project_id      :integer
#  user_id         :integer
#  organization_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class ProjectsUser < ApplicationRecord
	## Relationships
	belongs_to :user
	belongs_to :project
	belongs_to :organization
end
