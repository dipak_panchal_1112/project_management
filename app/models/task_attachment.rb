# == Schema Information
#
# Table name: task_attachments
#
#  id                    :integer          not null, primary key
#  task_id               :integer
#  user_id               :integer
#  project_id            :integer
#  document_file_name    :string
#  document_content_type :string
#  document_file_size    :integer
#  document_updated_at   :datetime
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  token                 :string
#

class TaskAttachment < ApplicationRecord
	## Relationships
	belongs_to :user
	belongs_to :project
	belongs_to :task
	
  before_destroy :add_histories_for_remove_attachment
  
	## Paperclip Attachment
  has_attached_file :document,
    storage: :s3,
    s3_region: 'us-west-2',
    s3_credentials: "#{Rails.root}/config/amazon_s3.yml",
    path: "/task-attachments/:id/:style/:filename",
    url: "/task-attachments/:id/:style/:filename"

  ## Callbacks
  after_create :add_histories_for_attachment

  def add_histories_for_attachment
    task.histories.create({
      user_id: Current.user.id, 
      message: "added attachment <span class='badge'>#{document_file_name}</span>",
      change_type: 'TaskUpdate'
    })
  end

  def add_histories_for_remove_attachment    
    task.histories.create({
      user_id: Current.user.id, 
      message: "remove attachment <span class='badge'>#{self.document_file_name}</span>",
      change_type: 'TaskUpdate'
    })    
  end

  ## Methods
  def created_on
  	created_at.strftime('%b %d, %Y')
  end

  def uniq_number
    unumber = "#{id}".rjust(2, '0')
    "TSKATCH0#{unumber}#{created_at.to_i}".downcase
  end
end
