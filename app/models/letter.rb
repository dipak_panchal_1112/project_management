# == Schema Information
#
# Table name: letters
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  description     :text
#  title           :string
#  is_default      :boolean          default(FALSE)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Letter < ApplicationRecord
	
	extend FriendlyId
  friendly_id :title, use: :slugged	

	## Relationship
	belongs_to :organization

	## Validations
	validates :description, :title, presence: true

	def should_generate_new_friendly_id?
		title_changed?
	end
end
