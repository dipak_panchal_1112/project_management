# == Schema Information
#
# Table name: subscriptions
#
#  id                    :integer          not null, primary key
#  plan_id               :integer
#  next_renewal_date     :date
#  credit_card_encrypted :string
#  card_expire           :string
#  subscription_id       :string
#  start_date            :date
#  end_date              :date
#  status                :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class Subscription < ApplicationRecord
	## Relationship
	has_one :organization
	has_many :transactions
end
