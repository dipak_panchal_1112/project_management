# == Schema Information
#
# Table name: profile2s
#
#  id                     :integer          not null, primary key
#  profile_id             :integer
#  company_name           :string
#  reason_for_resignation :text
#  start_date             :date
#  end_date               :date
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class Profile2 < ApplicationRecord
	belongs_to :profile
end
