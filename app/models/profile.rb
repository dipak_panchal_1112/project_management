# == Schema Information
#
# Table name: profiles
#
#  id                       :integer          not null, primary key
#  user_id                  :integer
#  phone_number             :string
#  mobile_number            :string
#  home_number              :string
#  address                  :string
#  city                     :string
#  state                    :string
#  zipcode                  :string
#  country                  :string
#  about_me                 :text
#  joining_date             :date
#  birth_date               :date
#  marriage_date            :date
#  resign_date              :date
#  website                  :string
#  avatar_file_name         :string
#  avatar_content_type      :string
#  avatar_file_size         :integer
#  avatar_updated_at        :datetime
#  designation              :string
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  organization_id          :integer
#  organizations_user_id    :integer
#  firstname                :string
#  lastname                 :string
#  employee_code            :string
#  gender                   :integer
#  education_status         :string
#  blood_group              :string
#  personal_email           :string
#  personal_skype           :string
#  skype                    :string
#  appointment_type         :integer          default(1)
#  bank_name                :string
#  account_number           :string
#  same_as_current_address  :boolean          default(FALSE)
#  permanent_address        :string
#  permanent_city           :string
#  permanent_state          :string
#  permanent_country        :string
#  permanent_zipcode        :string
#  parent_name              :string
#  relationship_with_parent :string
#  parent_phone_number      :string
#  parent_address           :string
#  parent_city              :string
#  parent_state             :string
#  parent_country           :string
#  parent_zipcode           :string
#  ifsc_code                :string
#

class Profile < ApplicationRecord
	## Relationship
	belongs_to :profile
  belongs_to :organizations_user
  belongs_to :organization
  belongs_to :user

  has_many :profile2s
  accepts_nested_attributes_for :profile2s, allow_destroy: true

  has_many :certificates, dependent: :destroy
  accepts_nested_attributes_for :certificates, allow_destroy: true

	## Profile Picture
  has_attached_file :avatar,
    styles: { small: ['48x48#', :png], medium: ['150x150#', :png]},
    storage: :s3,
    s3_region: 'us-west-2',
    s3_credentials: "#{Rails.root}/config/amazon_s3.yml",
    path: "/profile/:id/:style/:filename",
    url: "/profile/:id/:style/:filename"

  ## callback Methods
  before_create :generate_employee_code

  def generate_employee_code
    if organization.present?
      self.employee_code = "#{organization.profiles.count + 1 }"
    else
      self.employee_code = 1
    end
  end

  def emp_code
    "#{organization.name.upcase}-#{employee_code}"
  end

  def to_address    
    html = address
    if city.present? && state.present?
      html += "<br> #{city}, #{state}"
    end
    if country.present?
      html += "<br> #{country}"
    end
    if zipcode.present?
      html += "#{zipcode}"
    end
    html.html_safe if html.present?
  end

  def to_phone_number
    [phone_number, mobile_number, home_number].reject{|n| !n.present? }.join(', ')
  end

  def profile_image(size='small')
    if avatar.present?
      avatar.url(size.to_sym)
    else
      '/assets/not-found_m.jpg'
    end
  end

  def fullname
    "#{firstname.try(:humanize)} #{lastname.try(:humanize)}".strip
  end

  def my_appointment_type
    Rails.application.secrets.appointment_type[appointment_type]
  end

  def short_name
    firstname[0] + lastname[0]
  end
end
