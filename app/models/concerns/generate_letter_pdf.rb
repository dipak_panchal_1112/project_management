module GenerateLetterPdf
	def generate_pdf_for_letters(letter, param_letter, organization)
		employee 		= organization.organizations_users.find_by(user_id: param_letter['employee_id']).profile
		content 		= replace_tag_with_content(letter, param_letter, organization, employee)
		pdf 				= WickedPdf.new.pdf_from_string(content)				
		fileName 		= filename(letter, employee, organization)
		save_path 	= Rails.root.join('public/pdfs', fileName)
		letterFile 	= File.open(save_path, 'wb') do |file|
		  file << pdf					  
		end

		#File.open(letterFile.path, 'r') do |file|
		#	send_data file.read.force_encoding('BINARY'), filename: fileName, type: 'application/pdf'		
		#end
		redirect_to "/pdfs/#{fileName}"
	end

	def filename(letter, employee, organization)
		"#{organization.slug}-#{organization.id}-#{letter.title.remove(' ')}-for-#{employee.id}-#{employee.fullname.remove(' ')}.pdf"
	end

	def replace_tag_with_content(letter, param_letter, organization, employee)
		content = letter.description.html_safe
		if employee.fullname.present?
			content = content.gsub('[FULL-NAME]', employee.fullname).html_safe
		end
		if employee.designation.present?
			content = content.gsub('[POSITION]', employee.designation).html_safe
		end
		if organization.title.present?
			content = content.gsub('[COMPANY]', organization.title).html_safe
		end
		if employee.my_appointment_type.present?
			content = content.gsub('[APPOINTMENT-TYPE]', employee.my_appointment_type).html_safe
		end
		if employee.joining_date.present?
			content = content.gsub('[JOINING-DATE]', employee.joining_date).html_safe
		end

		if param_letter['supervisor_id'].present?
			supervisor = organization.organizations_users.find_by(user_id: param_letter['supervisor_id']).profile
			content = content.gsub('[SUPERVISOR]', supervisor.fullname)
			content = content.gsub('[SUPERVISOR-POSITION]', supervisor.designation)
		end

		if param_letter['sender_name'].present?
			content = content.gsub('[SENDER-NAME]', param_letter['sender_name'])
		end

		if param_letter['salary'].present?
			content = content.gsub('[SALARY]', param_letter['salary'])
		end

		return content.html_safe
	end

end