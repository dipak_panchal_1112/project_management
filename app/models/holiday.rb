# == Schema Information
#
# Table name: holidays
#
#  id              :integer          not null, primary key
#  name            :string
#  hdate           :date
#  htype           :integer
#  organization_id :integer
#  archive         :boolean
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Holiday < ApplicationRecord
	## Relationships
	belongs_to :organization

	## Validations
	validates :name, :hdate, :htype, presence: true

	## Attr Accessor
	attr_accessor :import_by, :file

	## scopes
	scope :nationals, -> {where('htype = ?', 1)}
	scope :generals, -> {where('htype = ?', 2)}

	## Method
	def holiday_type
		Rails.application.secrets.holiday_type[self.htype.to_i]
	end

	def uniq_number
		unumber = "#{id}".rjust(4, '0')
		"HLD0#{created_at.to_i}#{unumber}"
	end

end
