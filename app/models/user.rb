# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  firstname              :string
#  lastname               :string
#  username               :string           default(""), not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string
#  failed_attempts        :integer          default(0), not null
#  unlock_token           :string
#  locked_at              :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  slug                   :string
#  deleted_at             :datetime
#  organization_id        :integer
#  status_id              :integer          default(1)
#

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # , :omniauthable
  devise :database_authenticatable, :registerable, :confirmable, 
         :recoverable, :rememberable, :trackable, :validatable, :lockable, :timeoutable

  attr_accessor :login, :organization_name, :credit_card, :month_expire, :year_expire, :csc, :card_holder_name, :plan, :role

  ## Friendly ID
  extend FriendlyId
  friendly_id :username, use: :slugged

  ## Relationships
  has_many :organizations_users
	has_many :organizations, through: :organizations_users
  accepts_nested_attributes_for :organizations, allow_destroy: true
  accepts_nested_attributes_for :organizations_users, allow_destroy: true

  belongs_to :organization

  has_one :profile
  accepts_nested_attributes_for :profile

  ## Organization Owner and HR Manager can invite to other user to his/her organization.
  has_many :invitations, foreign_key: :invited_by_user_id, dependent: :destroy
  accepts_nested_attributes_for :invitations, allow_destroy: true

  has_many :projects_users
  has_many :projects, through: :projects_users
  
  has_many :task_attachments

  has_many :tasks_users
  has_many :tasks, through: :tasks_users

  has_many :work_logs
  
  has_many :watchers, dependent: :destroy
  has_many :profiles

  ## Callbacks
  before_create :add_organization_with_role_for_user
  before_create :create_profile

  ## For Session Controller - Authentication
  class << self
    def authenticate_organization_with_email_and_password(user_hash)
      login     = user_hash['login']
      password  = user_hash['password']
      
      return nil, 'both' if !login.present? && !password.present?
      return nil, 'login' unless login.present?
      return nil, 'password' unless password.present?
      
      if login.present? && password.present?
        user = User.where(['lower(username) = :value OR lower(email) = :value', { value: login.downcase }]).first
        return nil, 'User not found' unless user.present?

        if user.valid_password?(password)
          return user, nil
        else
          return nil, 'Password is invalid.'
        end
      end
    end
  
    ## Search users on organization owner page 
    def search(srch=nil)
      if srch.present?
        where('email LIKE ? or firstname LIKE ? or lastname LIKE ?', "%#{srch}%", "%#{srch}%", "%#{srch}%").order('created_at DESC')
      else
        order('created_at DESC')
      end
    end
  end

  ## Callback Methods ----------------------------------------------------------START 
  def add_organization_with_role_for_user
    if organization_name.present?
      organization = Organization.create(name: organization_name, plan: plan)
      self.organizations_users.build(organization_id: organization.id, role_id: 2)
      self.organization = organization
    end
  end

  def create_profile
    build_profile    
  end
  
  ## Boolean Methods -----------------------------------------------------------START
  def is_confirmed_user?
    !confirmed_at.nil?
  end

  ## Methods
  def fullname
    if firstname.present? && lastname.present?
      firstname.humanize + ' ' + lastname.humanize
    else
      username
    end
  end

  def profile_image(size='small')
    if profile.present? && profile.avatar.present?
      profile.avatar.url(size.to_sym)
    else
      '/assets/noimage.png'
    end
  end

  def status
    Rails.application.secrets.statuses[status_id]
  end

  def fullname_with_designation(organization)
    org_user = organizations_users.find_by(organization_id: organization.id)    
    profile = org_user.profile
    profile.designation.present? ? "#{profile.fullname} (#{profile.designation})" : profile.fullname
  end

end
