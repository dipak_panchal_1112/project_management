# == Schema Information
#
# Table name: organizations_users
#
#  id              :integer          not null, primary key
#  user_id         :integer
#  organization_id :integer
#  role_id         :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  status_id       :integer          default(1)
#

class OrganizationsUser < ApplicationRecord
	## Relationship
	belongs_to :user
	belongs_to :organization
	has_one :profile
	accepts_nested_attributes_for :profile, allow_destroy: true
	
	has_many :employee_documents
	accepts_nested_attributes_for :employee_documents, allow_destroy: true

	## Scopes
	scope :clients, -> {where('role_id = ?', 8)}
	## Methods

	def my_role
		Rails.application.secrets.roles[role_id]
	end

	def status
		Rails.application.secrets.statuses[status_id]
	end

	def active?
		status_id == 1
	end

  ## Search users on organization owner page 
  def self.search(srch=nil)
    if srch.present?
      joins(:user, :profile).where('users.email LIKE ? or lower(profiles.firstname) LIKE ? or lower(profiles.lastname) LIKE ?', "%#{srch}%", "%#{srch}%", "%#{srch}%").order('profiles.created_at DESC')
    else
      order('created_at DESC')
    end
  end

  def profile_image(size='small')
    if profile.present? && profile.avatar.present?
      profile.avatar.url(size.to_sym)
    else
      '/assets/noimage.png'
    end
  end
end
