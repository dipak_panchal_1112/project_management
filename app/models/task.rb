# == Schema Information
#
# Table name: tasks
#
#  id            :integer          not null, primary key
#  slug          :string
#  project_id    :integer
#  issue_type_id :integer          default(3)
#  summary       :text
#  description   :text
#  due_date      :date
#  status_id     :integer          default(1)
#  priority      :integer          default(1)
#  resolution    :integer
#  reporter_id   :integer
#  creator_id    :integer
#  total_min     :integer          default(0)
#  task_number   :integer          default(0)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  estimate_time :string
#  ancestry      :string
#

class Task < ApplicationRecord
	extend FriendlyId
  friendly_id :summary, use: :slugged
	
	has_ancestry
		
	## Relationships
	has_many :task_attachments, dependent: :destroy
	accepts_nested_attributes_for :task_attachments, allow_destroy: true

  has_many :tasks_users
  has_many :users, through: :tasks_users
  has_many :work_logs, dependent: :destroy

  has_many :comments, -> {order 'created_at DESC'}
	accepts_nested_attributes_for :comments, allow_destroy: true

	has_many :histories, -> {order 'created_at DESC'}

  belongs_to :project
  delegate :name, to: :project, prefix: true

  belongs_to :reporter, foreign_key: :reporter_id, class_name: 'User'
	
	has_many :watchers, dependent: :destroy
	  
	## Validations
	validates :summary, :project_id, presence: true

	## Callbacks
	before_create :set_task_number_as_project_wise
	## When task create we add entry in the history.
	after_create :add_histories_for_task
	before_save :add_histories_for_task_for_update

	## scopes
	scope :only_parents, -> {where('ancestry IS NULL')}
	scope :with_status_of, -> (id) { where(status_id: id) }

	## Callback Method
	def set_task_number_as_project_wise
		self.task_number 	= project.tasks.count + 1
		self.resolution 	= 1
	end

	def add_histories_for_task
		histories.create(user_id: Current.user.id, message: "created issue <b> #{self.summary}</b>", change_type: 'TaskCreate')
	end

	def add_histories_for_task_for_update		
		if self.summary_changed?
			histories.build(user_id: Current.user.id, message: "updated summary to <b> #{self.summary}</b>", change_type: 'SummaryUpdate')
		elsif self.description_changed?
			histories.build(user_id: Current.user.id, message: "updated description.", change_type: 'SummaryUpdate')
		elsif self.issue_type_id_changed?
			histories.build(user_id: Current.user.id, message: "changed issue type to <b> #{self.issue_type}</b>", change_type: 'TaskUpdate')
		elsif self.status_id_changed?
			histories.build(user_id: Current.user.id, message: "changed status to <b> #{self.status}</b>", change_type: 'TaskUpdate')
		elsif self.priority_changed?
			histories.build(user_id: Current.user.id, message: "changed priority to <b> #{self.task_priority}</b>", change_type: 'TaskUpdate')
		elsif self.resolution_changed?
			histories.build(user_id: Current.user.id, message: "changed resolution to <b> #{self.task_resolution}</b>", change_type: 'TaskUpdate')		
		elsif self.reporter_id_changed?
			histories.build(user_id: Current.user.id, message: "changed reporter to <b> #{self.reporter.fullname}</b>", change_type: 'TaskUpdate')
		end
	end

	## Other Methods
	def issue_type
		Rails.application.secrets.task_issue_types[issue_type_id]
	end

	def status
		Rails.application.secrets.task_statuses[status_id]
	end

	def task_priority
		Rails.application.secrets.priorities[priority]
	end

	def code
		"#{project.key}-#{task_number}"
	end

  def self.search(params=nil)
    if params.present?
    	query = []
      query << "priority = #{params['priority']}" if params['priority'].present? 
      query << "issue_type_id = #{params['type']}" if params['type'].present?
      query << "status_id = #{params['status']}" if params['status'].present?
      query << "resolution = #{params['resolution']}" if params['resolution'].present?
      query << "lower(summary) LIKE '%#{params['search'].downcase}%'" if params['search'].present?
      sort_by = params['sort_by'].present? ? params[:sort_by] : 'created_at'
      where(query.join(' AND ')).order("#{sort_by} ASC")      
    else
      order('created_at DESC')
    end
  end

  def task_resolution
  	Rails.application.secrets.task_resolutions[resolution]
  end

  def created_on
  	created_at.strftime('%d/%b/%y %H:%m %p')
  end

	def updated_on
  	updated_at.strftime('%d/%b/%y %H:%m %p')
  end

  def self.generate_token
  	begin
    	return SecureRandom.urlsafe_base64 10
  	end while TaskAttachment.exists?(token: token)
	end

	def my_hours(user)
		mtask = tasks_users.find_by(user_id: user.id)		
		return min_to_time_format(mtask.total_min)
	end

	def total_time
		min_to_time_format(total_min)		
	end

	def min_to_time_format(min)
		hh, mm = min.divmod(60)
		dd, hh = hh.divmod(24)
		ss = 00
		return "#{hh}:#{mm}:#{ss}"
	end

	def status_color
		case status_id
		when 1
			'#d3d3d3'
		when 2
			'#bababa'
		when 3
			'#79d2c0'
		when 4
			'#1ab394'
		when 5
			'#18a689'
		when 6
			'#0a5a4a'
		when 7
			'#58988b'
		end
	end

	def issue_type_color
		case issue_type_id
		when 1
			'#d3d3d3'
		when 2
			'#bababa'
		when 3
			'#79d2c0'
		when 4
			'#1ab394'
		when 5
			'#18a689'
		when 6
			'#0a5a4a'
		end		
	end
end
