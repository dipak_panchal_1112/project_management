# == Schema Information
#
# Table name: plans
#
#  id         :integer          not null, primary key
#  name       :string
#  price      :float            default(0.0)
#  title      :string
#  users      :integer          default(0)
#  storage    :integer          default(0)
#  primary    :boolean          default(FALSE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  deleted_at :datetime
#

class Plan < ApplicationRecord

	## For Soft Delete
	acts_as_paranoid
	
	## Relationships
	has_many :organizations, -> { order(:created_at) }

	## Validation
	validates :name, :title, :price, presence: true
	
	## Methods
	def plan_amount
		"$ #{price}"
	end

end
