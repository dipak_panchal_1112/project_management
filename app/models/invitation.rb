# == Schema Information
#
# Table name: invitations
#
#  id                         :integer          not null, primary key
#  token                      :string
#  invitation_sent_at         :datetime
#  invitation_accepted_at     :datetime
#  invited_by_user_id         :integer
#  invited_by_organization_id :integer
#  invite_email               :string
#  role_id                    :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#

class Invitation < ApplicationRecord
	## Relationship
	belongs_to :invited_by_user, foreign_key: :invited_by_user_id, class_name: 'User'
	belongs_to :invited_by_organization, foreign_key: :invited_by_organization_id, class_name: 'Organization'

	## Validations
	validates :invite_email, :role_id, presence: true
	validates :invite_email, uniqueness: {scope: :invited_by_organization_id}

	## Scopes
	scope :with_tokens, -> {where('token IS NOT NULL')}
	## Callbacks
	before_create :generate_token, :set_invitation_sent_at
	after_create :send_invitation_email

	def role
		Rails.application.secrets.display_roles[role_id]
	end

	def uniq_number
		unumber = "#{id}".rjust(4, '0')
		"INV0#{created_at.to_i}#{unumber}"
	end

	protected
	
	## Callback Methods
	def generate_token
		self.token = loop do
      random_token = SecureRandom.urlsafe_base64(nil, false)
      break random_token unless Invitation.exists?(token: random_token)
    end		
	end

	def set_invitation_sent_at
		self.invitation_sent_at = Time.zone.now
	end

	def send_invitation_email
	  InvitationMailer.send_invitation(self).deliver_now
	end

end
