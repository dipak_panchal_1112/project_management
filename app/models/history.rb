# == Schema Information
#
# Table name: histories
#
#  id          :integer          not null, primary key
#  task_id     :integer
#  user_id     :integer
#  message     :text
#  change_type :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class History < ApplicationRecord
	## Relationship
	belongs_to :task
	belongs_to :user	
end
