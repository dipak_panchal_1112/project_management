# == Schema Information
#
# Table name: watchers
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  task_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Watcher < ApplicationRecord
	## Relationship
	belongs_to :task
	belongs_to :user

	## Methods

	def self.update_watchlist(params, current_user, task)
		user_id = params[:watcher].present? ? params[:watcher][:user_id] : current_user.id
		watcher = Watcher.find_by(task_id: task.id, user_id: user_id)
		if watcher.present?
			watcher.destroy
		else
			task.watchers.create(user_id: user_id)
		end
	end

end
